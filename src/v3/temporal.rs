//! Temporal vector implementation.

use crate::parsing;
use crate::parsing::{Field, OptionalParser};
use crate::v3::roundup::roundup;
use serde::{Deserialize, Serialize};
use std::fmt::{Error, Formatter};
use std::str::Split;

/// A CVSS V3.0 temporal vector.
///
/// Per the CVSS specification, this structure contains an Exploit Code Maturity field, a Remediation Level field, and a Report Confidence field, all optionals.
#[derive(Debug, Clone, Copy, PartialEq, Deserialize, Serialize)]
pub struct TemporalVector {
    /// Optional Exploit Code Maturity field as defined by the CVSS specification.
    pub exploit_code_maturity: Option<ExploitCodeMaturity>,
    /// Optional Remediation Level field as defined by the CVSS specification.
    pub remediation_level: Option<RemediationLevel>,
    /// Optional Report Confidence field as defined by the CVSS specification.
    pub report_confidence: Option<ReportConfidence>,
}

/// The Exploit Code Maturity field as defined by the CVSS specification.
///
/// "Not Defined" is considered the same as a missing value. This is done by setting [`TemporalVector.exploit_code_maturity`] to [`None`].
///
/// [`TemporalVector.exploit_code_maturity`]: struct.TemporalVector.html#structfield.exploit_code_maturity
/// [`None`]: https://doc.rust-lang.org/std/option/enum.Option.html#variant.None
#[derive(Debug, Clone, Copy, PartialEq, Deserialize, Serialize)]
pub enum ExploitCodeMaturity {
    // "Not Defined" is considered the same as a missing value. This is done by setting Temporal.exploit_code_maturity to None.
    /// High ("E:H") value for the Exploit Code Maturity field.
    High,
    /// Functional ("E:F") value for the Exploit Code Maturity field.
    Functional,
    /// Proof Of Concept ("E:P") value for the Exploit Code Maturity field.
    ProofOfConcept,
    /// Unproven ("E:U") value for the Exploit Code Maturity field.
    Unproven,
}

/// The Remediation Level field as defined by the CVSS specification.
///
/// "Not Defined" is considered the same as a missing value. This is done by setting [`TemporalVector.remediation_level`] to [`None`].
///
/// [`TemporalVector.remediation_level`]: struct.TemporalVector.html#structfield.remediation_level
/// [`None`]: https://doc.rust-lang.org/std/option/enum.Option.html#variant.None
#[derive(Debug, Clone, Copy, PartialEq, Deserialize, Serialize)]
pub enum RemediationLevel {
    // "Not Defined" is considered the same as a missing value. This is done by setting Temporal.remediation_level to None.
    /// Unavailable ("RL:U") value for the Remediation Level field.
    Unavailable,
    /// Workaround ("RL:W") value for the Remediation Level field.
    Workaround,
    /// Temporary Fix ("RL:T") value for the Remediation Level field.
    TemporaryFix,
    /// Official Fix ("RL:O") value for the Remediation Level field.
    OfficialFix,
}

/// The Report Confidence field as defined by the CVSS specification.
///
/// "Not Defined" is considered the same as a missing value. This is done by setting [`TemporalVector.report_confidence`] to [`None`].
///
/// [`TemporalVector.report_confidence`]: struct.TemporalVector.html#structfield.report_confidence
/// [`None`]: https://doc.rust-lang.org/std/option/enum.Option.html#variant.None
#[derive(Debug, Clone, Copy, PartialEq, Deserialize, Serialize)]
pub enum ReportConfidence {
    // "Not Defined" is considered the same as a missing value. This is done by setting Temporal.remediation_level to None.
    /// Confirmed ("RC:C") value for the Report Confidence field.
    Confirmed,
    /// Reasonable ("RC:R") value for the Report Confidence field.
    Reasonable,
    /// Unknown ("RC:U") value for the Report Confidence field.
    Unknown,
}

impl TemporalVector {
    /// Provides the severity score for the CVSS temporal vector, given a base score as input.
    ///
    /// This score respects the CVSS 3.0 specification, particularly regarding floating-point roundup.
    ///
    /// Calling this method is identical to calling [`CVSS3Vector.score()`] when [`CVSS3Vector.environmental`] is set to [`None`].
    ///
    /// [`CVSS3Vector.score()`]: ../struct.CVSS3Vector.html#method.score
    /// [`CVSS3Vector.environmental`]: ../struct.CVSS3Vector.html#structfield.environmental
    /// [`None`]: https://doc.rust-lang.org/std/option/enum.Option.html#variant.None
    pub fn score(&self, base_score: f64) -> f64 {
        let mut result = base_score;

        result = match self.exploit_code_maturity {
            None => result,
            Some(field) => result * field.numerical_value(),
        };

        result = match self.remediation_level {
            None => result,
            Some(field) => result * field.numerical_value(),
        };

        result = match self.report_confidence {
            None => result,
            Some(field) => result * field.numerical_value(),
        };

        roundup(result)
    }
}

impl OptionalParser for TemporalVector {
    fn parse_optional(split: &Box<Split<char>>) -> Option<Self>
    where
        Self: Sized,
    {
        let res = TemporalVector {
            exploit_code_maturity: ExploitCodeMaturity::parse_optional(split),
            remediation_level: RemediationLevel::parse_optional(split),
            report_confidence: ReportConfidence::parse_optional(split),
        };

        if res.exploit_code_maturity.is_none()
            && res.remediation_level.is_none()
            && res.report_confidence.is_none()
        {
            None
        } else {
            Some(res)
        }
    }
}

impl ExploitCodeMaturity {
    /// Provides the numerical value associated with the metric, as specified by the CVSS specification.
    ///
    /// In normal usage you should not need to call this yourself.
    pub fn numerical_value(&self) -> f64 {
        match *self {
            ExploitCodeMaturity::High => 1.0,
            ExploitCodeMaturity::Functional => 0.97,
            ExploitCodeMaturity::ProofOfConcept => 0.94,
            ExploitCodeMaturity::Unproven => 0.91,
        }
    }
}

impl Field for ExploitCodeMaturity {
    fn abbreviated_form(&self) -> &'static str {
        match *self {
            ExploitCodeMaturity::High => "E:H",
            ExploitCodeMaturity::Functional => "E:F",
            ExploitCodeMaturity::ProofOfConcept => "E:P",
            ExploitCodeMaturity::Unproven => "E:U",
        }
    }

    fn error_message() -> &'static str {
        // We do not define any error message for optional fields
        unreachable!()
    }

    fn parse(input: &str) -> Option<ExploitCodeMaturity> {
        if input == ExploitCodeMaturity::High.abbreviated_form() {
            Some(ExploitCodeMaturity::High)
        } else if input == ExploitCodeMaturity::Functional.abbreviated_form() {
            Some(ExploitCodeMaturity::Functional)
        } else if input == ExploitCodeMaturity::ProofOfConcept.abbreviated_form() {
            Some(ExploitCodeMaturity::ProofOfConcept)
        } else if input == ExploitCodeMaturity::Unproven.abbreviated_form() {
            Some(ExploitCodeMaturity::Unproven)
        } else {
            None
        }
    }
}

impl RemediationLevel {
    /// Provides the numerical value associated with the metric, as specified by the CVSS specification.
    ///
    /// In normal usage you should not need to call this yourself.
    pub fn numerical_value(&self) -> f64 {
        match *self {
            RemediationLevel::Unavailable => 1.0,
            RemediationLevel::Workaround => 0.97,
            RemediationLevel::TemporaryFix => 0.96,
            RemediationLevel::OfficialFix => 0.95,
        }
    }
}

impl Field for RemediationLevel {
    fn abbreviated_form(&self) -> &'static str {
        match *self {
            RemediationLevel::Unavailable => "RL:U",
            RemediationLevel::Workaround => "RL:W",
            RemediationLevel::TemporaryFix => "RL:T",
            RemediationLevel::OfficialFix => "RL:O",
        }
    }

    fn error_message() -> &'static str {
        // We do not define any error message for optional fields
        unreachable!()
    }

    fn parse(input: &str) -> Option<RemediationLevel> {
        if input == RemediationLevel::Unavailable.abbreviated_form() {
            Some(RemediationLevel::Unavailable)
        } else if input == RemediationLevel::Workaround.abbreviated_form() {
            Some(RemediationLevel::Workaround)
        } else if input == RemediationLevel::TemporaryFix.abbreviated_form() {
            Some(RemediationLevel::TemporaryFix)
        } else if input == RemediationLevel::OfficialFix.abbreviated_form() {
            Some(RemediationLevel::OfficialFix)
        } else {
            None
        }
    }
}

impl ReportConfidence {
    /// Provides the numerical value associated with the metric, as specified by the CVSS specification.
    ///
    /// In normal usage you should not need to call this yourself.
    pub fn numerical_value(&self) -> f64 {
        match *self {
            ReportConfidence::Confirmed => 1.0,
            ReportConfidence::Reasonable => 0.96,
            ReportConfidence::Unknown => 0.92,
        }
    }
}

impl Field for ReportConfidence {
    fn abbreviated_form(&self) -> &'static str {
        match *self {
            ReportConfidence::Confirmed => "RC:C",
            ReportConfidence::Reasonable => "RC:R",
            ReportConfidence::Unknown => "RC:U",
        }
    }

    fn error_message() -> &'static str {
        // We do not define any error message for optional fields
        unreachable!()
    }

    fn parse(input: &str) -> Option<ReportConfidence> {
        if input == ReportConfidence::Confirmed.abbreviated_form() {
            Some(ReportConfidence::Confirmed)
        } else if input == ReportConfidence::Reasonable.abbreviated_form() {
            Some(ReportConfidence::Reasonable)
        } else if input == ReportConfidence::Unknown.abbreviated_form() {
            Some(ReportConfidence::Unknown)
        } else {
            None
        }
    }
}

impl OptionalParser for ExploitCodeMaturity {
    fn parse_optional(split: &Box<Split<char>>) -> Option<Self>
    where
        Self: Sized,
    {
        parsing::parse_optional_default_field(split)
    }
}

impl OptionalParser for RemediationLevel {
    fn parse_optional(split: &Box<Split<char>>) -> Option<Self>
    where
        Self: Sized,
    {
        parsing::parse_optional_default_field(split)
    }
}

impl OptionalParser for ReportConfidence {
    fn parse_optional(split: &Box<Split<char>>) -> Option<Self>
    where
        Self: Sized,
    {
        parsing::parse_optional_default_field(split)
    }
}

impl std::fmt::Display for ExploitCodeMaturity {
    fn fmt(&self, f: &mut Formatter) -> Result<(), Error> {
        write!(f, "{}", self.abbreviated_form())
    }
}

impl std::fmt::Display for RemediationLevel {
    fn fmt(&self, f: &mut Formatter) -> Result<(), Error> {
        write!(f, "{}", self.abbreviated_form())
    }
}

impl std::fmt::Display for ReportConfidence {
    fn fmt(&self, f: &mut Formatter) -> Result<(), Error> {
        write!(f, "{}", self.abbreviated_form())
    }
}

impl std::fmt::Display for TemporalVector {
    fn fmt(&self, f: &mut Formatter) -> Result<(), Error> {
        // Fancy way to write a/b/c which degrade to a/b, b/c, a etc. when any field is missing (as they are all optional)
        let mut fields = Vec::new();

        match self.exploit_code_maturity {
            None => (),
            Some(field) => fields.push(field.abbreviated_form()),
        };

        match self.remediation_level {
            None => (),
            Some(field) => fields.push(field.abbreviated_form()),
        };

        match self.report_confidence {
            None => (),
            Some(field) => fields.push(field.abbreviated_form()),
        };

        write!(f, "{}", fields.join("/"))
    }
}

#[cfg(test)]
pub mod tests {
    use super::*;

    #[test]
    fn test_formatting() {
        // Exploit Code Maturity
        assert_eq!("E:H", format!("{}", ExploitCodeMaturity::High));
        assert_eq!("E:F", format!("{}", ExploitCodeMaturity::Functional));
        assert_eq!("E:P", format!("{}", ExploitCodeMaturity::ProofOfConcept));
        assert_eq!("E:U", format!("{}", ExploitCodeMaturity::Unproven));

        // Remediation Level
        assert_eq!("RL:U", format!("{}", RemediationLevel::Unavailable));
        assert_eq!("RL:W", format!("{}", RemediationLevel::Workaround));
        assert_eq!("RL:T", format!("{}", RemediationLevel::TemporaryFix));
        assert_eq!("RL:O", format!("{}", RemediationLevel::OfficialFix));

        // Report Confidence
        assert_eq!("RC:C", format!("{}", ReportConfidence::Confirmed));
        assert_eq!("RC:R", format!("{}", ReportConfidence::Reasonable));
        assert_eq!("RC:U", format!("{}", ReportConfidence::Unknown));

        // Temporal vector
        assert_eq!("E:F/RL:U/RC:R", format!("{}", provide_temporal_vector1()));

        // Missing fields
        let mut missing_fields = provide_temporal_vector1();
        missing_fields.exploit_code_maturity = None;
        assert_eq!("RL:U/RC:R", format!("{}", missing_fields));
    }

    #[test]
    fn test_parsing() {
        // Exploit Code Maturity
        assert_eq!(
            Some(ExploitCodeMaturity::High),
            ExploitCodeMaturity::parse("E:H")
        );
        assert_eq!(
            Some(ExploitCodeMaturity::Functional),
            ExploitCodeMaturity::parse("E:F")
        );
        assert_eq!(
            Some(ExploitCodeMaturity::ProofOfConcept),
            ExploitCodeMaturity::parse("E:P")
        );
        assert_eq!(
            Some(ExploitCodeMaturity::Unproven),
            ExploitCodeMaturity::parse("E:U")
        );
        assert_eq!(None, ExploitCodeMaturity::parse("fslfjskfj"));
        assert_eq!(None, ExploitCodeMaturity::parse("E:"));
        assert_eq!(None, ExploitCodeMaturity::parse("E:HF"));

        // Remediation Level
        assert_eq!(
            Some(RemediationLevel::Unavailable),
            RemediationLevel::parse("RL:U")
        );
        assert_eq!(
            Some(RemediationLevel::Workaround),
            RemediationLevel::parse("RL:W")
        );
        assert_eq!(
            Some(RemediationLevel::TemporaryFix),
            RemediationLevel::parse("RL:T")
        );
        assert_eq!(
            Some(RemediationLevel::OfficialFix),
            RemediationLevel::parse("RL:O")
        );
        assert_eq!(None, RemediationLevel::parse("fslfjskfj"));
        assert_eq!(None, RemediationLevel::parse("RL:"));
        assert_eq!(None, RemediationLevel::parse("RL:UW"));

        // Report Confidence
        assert_eq!(
            Some(ReportConfidence::Confirmed),
            ReportConfidence::parse("RC:C")
        );
        assert_eq!(
            Some(ReportConfidence::Reasonable),
            ReportConfidence::parse("RC:R")
        );
        assert_eq!(
            Some(ReportConfidence::Unknown),
            ReportConfidence::parse("RC:U")
        );
        assert_eq!(None, ReportConfidence::parse("fslfjskfj"));
        assert_eq!(None, ReportConfidence::parse("RC:"));
        assert_eq!(None, ReportConfidence::parse("RC:CR"));

        // Temporal Vector
        // Happy path
        assert_eq!(
            Some(provide_temporal_vector1()),
            TemporalVector::parse_optional(&mut Box::new("E:F/RL:U/RC:R".split('/'))),
        );

        // Wrong order
        assert_eq!(
            Some(provide_temporal_vector1()),
            TemporalVector::parse_optional(&mut Box::new("RL:U/E:F/RC:R".split('/'))),
        );

        // Missing fields
        let mut missing_fields = provide_temporal_vector1();
        missing_fields.exploit_code_maturity = None;
        assert_eq!(
            Some(missing_fields),
            TemporalVector::parse_optional(&mut Box::new("RL:U/RC:R".split('/'))),
        );

        // Junk
        assert_eq!(
            None,
            TemporalVector::parse_optional(&mut Box::new("fsjfskhf".split('/'))),
        );
        assert_eq!(
            None,
            TemporalVector::parse_optional(&mut Box::new("fs//jf/skhf".split('/'))),
        );
    }

    #[test]
    fn test_scoring() {
        assert_eq!(9.4, provide_temporal_vector1().score(10.0));
    }

    // This example is invented from scratch as the CVSS 3.0 specification does not provide a temporal vector example.
    pub fn provide_temporal_vector1() -> TemporalVector {
        TemporalVector {
            exploit_code_maturity: Some(ExploitCodeMaturity::Functional),
            remediation_level: Some(RemediationLevel::Unavailable),
            report_confidence: Some(ReportConfidence::Reasonable),
        }
    }
}
