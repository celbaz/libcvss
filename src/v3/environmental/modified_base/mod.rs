//! Modified Base sub-vector implementation.

use crate::parsing::OptionalParser;
use crate::v3::base::scope::Scope;
use crate::v3::base::BaseVector;
use crate::v3::environmental::modified_base::modified_exploitability::ModifiedExploitability;
use crate::v3::environmental::modified_base::modified_impact::ModifiedImpact;
use crate::v3::environmental::modified_base::modified_scope::ModifiedScope;
use crate::v3::environmental::security_requirements::SecurityRequirements;
use crate::v3::roundup::roundup;
use crate::v3::temporal::TemporalVector;
use serde::{Deserialize, Serialize};
use std::fmt::{Error, Formatter};
use std::str::Split;

pub mod modified_exploitability;
pub mod modified_impact;
pub mod modified_scope;

/// A CVSS V3.0 Modified Base sub-vector.
///
/// Per the CVSS specification, this structure contains a Modified Exploitability sub-vector, a Modified Scope field, and a Modified Impact sub-vector, all optionals.
#[derive(Debug, Clone, Copy, PartialEq, Deserialize, Serialize)]
pub struct ModifiedBaseVector {
    /// Optional Modified Exploitability sub-vector as defined by the CVSS specification.
    pub modified_exploitability: Option<ModifiedExploitability>,
    /// Optional Modified Scope sub-vector as defined by the CVSS specification.
    pub modified_scope: Option<ModifiedScope>,
    /// Optional Modified Impact sub-vector as defined by the CVSS specification.
    pub modified_impact: Option<ModifiedImpact>,
}

impl ModifiedBaseVector {
    #[doc(hidden)]
    pub fn score(
        base_vector: BaseVector,
        temporal_vector: Option<TemporalVector>,
        modified_base_vector: Option<ModifiedBaseVector>,
        security_requirements: Option<SecurityRequirements>,
    ) -> f64 {
        let modified_impact_score = ModifiedImpact::score(
            base_vector.impact,
            base_vector.scope,
            match modified_base_vector {
                None => None,
                Some(modified_base) => modified_base.modified_impact,
            },
            security_requirements,
            match modified_base_vector {
                None => None,
                Some(modified_base) => modified_base.modified_scope,
            },
        );

        if modified_impact_score <= 0.0 {
            return 0.0;
        }

        let scope_changed = match modified_base_vector {
            None => match base_vector.scope {
                Scope::Unchanged => false,
                Scope::Changed => true,
            },
            Some(modified_base) => match modified_base.modified_scope {
                None => match base_vector.scope {
                    Scope::Unchanged => false,
                    Scope::Changed => true,
                },
                Some(scope) => match scope {
                    ModifiedScope::Unchanged => false,
                    ModifiedScope::Changed => true,
                },
            },
        };

        let modified_exploitability_score = ModifiedExploitability::score(
            base_vector.exploitability,
            base_vector.scope,
            match modified_base_vector {
                None => None,
                Some(modified_base) => modified_base.modified_exploitability,
            },
            match modified_base_vector {
                None => None,
                Some(modified_base) => modified_base.modified_scope,
            },
        );

        let (exploit_code_maturity_score, remediation_level_score, report_confidence_score) =
            match temporal_vector {
                None => (1.0, 1.0, 1.0),
                Some(temporal) => (
                    match temporal.exploit_code_maturity {
                        None => 1.0,
                        Some(exploit_code_maturity) => exploit_code_maturity.numerical_value(),
                    },
                    match temporal.remediation_level {
                        None => 1.0,
                        Some(remediation_level) => remediation_level.numerical_value(),
                    },
                    match temporal.report_confidence {
                        None => 1.0,
                        Some(report_confidence) => report_confidence.numerical_value(),
                    },
                ),
            };

        if scope_changed {
            roundup(
                roundup(f64::min(
                    1.08 * (modified_impact_score + modified_exploitability_score),
                    10.0,
                )) * exploit_code_maturity_score
                    * remediation_level_score
                    * report_confidence_score,
            )
        } else {
            roundup(
                roundup(f64::min(
                    modified_impact_score + modified_exploitability_score,
                    10.0,
                )) * exploit_code_maturity_score
                    * remediation_level_score
                    * report_confidence_score,
            )
        }
    }
}

impl OptionalParser for ModifiedBaseVector {
    fn parse_optional(split: &Box<Split<char>>) -> Option<Self>
    where
        Self: Sized,
    {
        let res = ModifiedBaseVector {
            modified_exploitability: ModifiedExploitability::parse_optional(split),
            modified_scope: ModifiedScope::parse_optional(split),
            modified_impact: ModifiedImpact::parse_optional(split),
        };

        if res.modified_exploitability.is_none()
            && res.modified_scope.is_none()
            && res.modified_impact.is_none()
        {
            None
        } else {
            Some(res)
        }
    }
}

impl std::fmt::Display for ModifiedBaseVector {
    fn fmt(&self, f: &mut Formatter) -> Result<(), Error> {
        // Fancy way to write a/b/c which degrade to a/b, b/c, a etc. when any field is missing (as they are all optional)
        let mut fields = Vec::new();

        match self.modified_exploitability {
            None => (),
            Some(field) => fields.push(format!("{}", field)),
        };

        match self.modified_scope {
            None => (),
            Some(field) => fields.push(format!("{}", field)),
        };

        match self.modified_impact {
            None => (),
            Some(field) => fields.push(format!("{}", field)),
        };

        write!(f, "{}", fields.join("/"))
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::v3::base::exploitability::{
        AttackComplexity, AttackVector, Exploitability, PrivilegesRequired, UserInteraction,
    };
    use crate::v3::base::impact::{Availability, Confidentiality, Impact, Integrity};
    use crate::v3::base::scope::Scope;
    use crate::v3::base::BaseVector;
    use crate::v3::environmental::modified_base::modified_exploitability::{
        ModifiedAttackComplexity, ModifiedAttackVector,
    };
    use crate::v3::environmental::security_requirements::{
        AvailabilityRequirement, ConfidentialityRequirement, IntegrityRequirement,
        SecurityRequirements,
    };
    use crate::v3::temporal::{
        ExploitCodeMaturity, RemediationLevel, ReportConfidence, TemporalVector,
    };

    #[test]
    fn test_formatting() {
        assert_eq!(
            "MAV:N/MAC:L/MPR:N/MUI:N/MS:U/MC:L/MI:H/MA:N",
            format!("{}", provide_modified_base_vector1())
        );
    }

    #[test]
    fn test_parsing() {
        // This example is invented from scratch as the CVSS 3.0 specification does not provide a temporal vector example.
        assert_eq!(
            Some(provide_modified_base_vector1()),
            ModifiedBaseVector::parse_optional(&mut Box::new(
                "MAV:N/MAC:L/MPR:N/MUI:N/MS:U/MC:L/MI:H/MA:N".split('/')
            )),
        );

        // With two fields swapped inside a subvector (exploitability)
        assert_eq!(
            Some(provide_modified_base_vector1()),
            ModifiedBaseVector::parse_optional(&mut Box::new(
                "MAC:L/MAV:N/MPR:N/MUI:N/MS:U/MC:L/MI:H/MA:N".split('/')
            )),
        );

        // With two fields swapped between two subvector (exploitability and impact)
        assert_eq!(
            Some(provide_modified_base_vector1()),
            ModifiedBaseVector::parse_optional(&mut Box::new(
                "MAC:L/MPR:N/MUI:N/MS:U/MC:L/MI:H/MA:N/MAV:N".split('/')
            )),
        );
        assert_eq!(
            Some(provide_modified_base_vector1()),
            ModifiedBaseVector::parse_optional(&mut Box::new(
                "MS:U/MAV:N/MAC:L/MPR:N/MUI:N/MC:L/MI:H/MA:N/ME:F/MRL:X".split('/')
            )),
        );

        // Junk entry
        assert_eq!(
            None,
            ModifiedBaseVector::parse_optional(&mut Box::new("fsjfskhf".split('/')))
        );
        assert_eq!(
            None,
            ModifiedBaseVector::parse_optional(&mut Box::new("fs//jf/skhf".split('/')))
        );
    }

    #[test]
    fn test_scoring() {
        let base_vector = BaseVector {
            exploitability: Exploitability {
                attack_vector: AttackVector::Adjacent,
                attack_complexity: AttackComplexity::High,
                privileges_required: PrivilegesRequired::None,
                user_interaction: UserInteraction::None,
            },
            scope: Scope::Unchanged,
            impact: Impact {
                confidentiality: Confidentiality::High,
                integrity: Integrity::High,
                availability: Availability::High,
            },
        };

        let temporal_vector = TemporalVector {
            exploit_code_maturity: Some(ExploitCodeMaturity::Functional),
            remediation_level: Some(RemediationLevel::OfficialFix),
            report_confidence: Some(ReportConfidence::Confirmed),
        };

        let modified_base_vector = ModifiedBaseVector {
            modified_exploitability: Some(ModifiedExploitability {
                modified_attack_vector: Some(ModifiedAttackVector::Network),
                modified_attack_complexity: Some(ModifiedAttackComplexity::Low),
                modified_privileges_required: None,
                modified_user_interaction: None,
            }),
            modified_scope: None,
            modified_impact: None,
        };

        let security_requirements = SecurityRequirements {
            confidentiality_requirement: Some(ConfidentialityRequirement::High),
            integrity_requirement: Some(IntegrityRequirement::High),
            availability_requirement: Some(AvailabilityRequirement::Medium),
        };

        assert_eq!(
            9.1,
            ModifiedBaseVector::score(
                base_vector,
                Some(temporal_vector),
                Some(modified_base_vector),
                Some(security_requirements)
            )
        );
    }

    // This example is invented from scratch as the CVSS 3.0 specification does not provide a temporal vector example.
    fn provide_modified_base_vector1() -> ModifiedBaseVector {
        ModifiedBaseVector {
            modified_exploitability: Some(
                modified_exploitability::tests::provide_modified_exploitability_vector1(),
            ),
            modified_scope: Some(ModifiedScope::Unchanged),
            modified_impact: Some(modified_impact::tests::provide_modified_impact_vector1()),
        }
    }
}
