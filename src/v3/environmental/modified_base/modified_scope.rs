//! Modified Scope field implementation.

use crate::parsing;
use crate::parsing::{Field, OptionalParser};
use serde::{Deserialize, Serialize};
use std::fmt::{Error, Formatter};
use std::str::Split;

/// The Modified Scope field as defined by the CVSS specification.
#[derive(Debug, Clone, Copy, PartialEq, Deserialize, Serialize)]
pub enum ModifiedScope {
    /// Unchanged ("MS:U") value for the Modified Scope field.
    Unchanged,
    /// Changed ("MS:C") value for the Modified Scope field.
    Changed,
}

impl Field for ModifiedScope {
    fn abbreviated_form(&self) -> &'static str {
        match *self {
            ModifiedScope::Unchanged => "MS:U",
            ModifiedScope::Changed => "MS:C",
        }
    }

    fn error_message() -> &'static str {
        // We do not define any error message for optional fields
        unreachable!()
    }

    fn parse(input: &str) -> Option<ModifiedScope> {
        if input == ModifiedScope::Unchanged.abbreviated_form() {
            Some(ModifiedScope::Unchanged)
        } else if input == ModifiedScope::Changed.abbreviated_form() {
            Some(ModifiedScope::Changed)
        } else {
            None
        }
    }
}

impl OptionalParser for ModifiedScope {
    fn parse_optional(split: &Box<Split<char>>) -> Option<Self>
    where
        Self: Sized,
    {
        parsing::parse_optional_default_field(split)
    }
}

impl std::fmt::Display for ModifiedScope {
    fn fmt(&self, f: &mut Formatter) -> Result<(), Error> {
        write!(f, "{}", self.abbreviated_form())
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_formatting() {
        assert_eq!("MS:U", format!("{}", ModifiedScope::Unchanged));
        assert_eq!("MS:C", format!("{}", ModifiedScope::Changed));
    }

    #[test]
    fn test_parsing() {
        assert_eq!(Some(ModifiedScope::Unchanged), ModifiedScope::parse("MS:U"));
        assert_eq!(Some(ModifiedScope::Changed), ModifiedScope::parse("MS:C"));

        assert_eq!(None, ModifiedScope::parse("fsfdjslkdfjs"));
        assert_eq!(None, ModifiedScope::parse("MS:UC"));
    }
}
