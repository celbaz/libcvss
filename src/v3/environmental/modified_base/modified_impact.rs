//! Modified Impact sub-vector implementation.

use crate::parsing;
use crate::parsing::{Field, OptionalParser};
use crate::v3::base::impact::Impact;
use crate::v3::base::scope::Scope;
use crate::v3::environmental::modified_base::modified_scope::ModifiedScope;
use crate::v3::environmental::security_requirements::SecurityRequirements;
use serde::{Deserialize, Serialize};
use std::fmt::{Error, Formatter};
use std::str::Split;

/// The Modified Impact sub-vector as defined by the CVSS specification.
#[derive(Debug, Clone, Copy, PartialEq, Deserialize, Serialize)]
pub struct ModifiedImpact {
    /// Optional Modified Confidentiality field as defined by the CVSS specification.
    pub modified_confidentiality: Option<ModifiedConfidentiality>,
    /// Optional Modified Integrity field as defined by the CVSS specification.
    pub modified_integrity: Option<ModifiedIntegrity>,
    /// Optional Modified Availability field as defined by the CVSS specification.
    pub modified_availability: Option<ModifiedAvailability>,
}

/// The Modified Confidentiality field as defined by the CVSS specification.
#[derive(Debug, Clone, Copy, PartialEq, Deserialize, Serialize)]
pub enum ModifiedConfidentiality {
    /// High ("MC:H") value for the Modified Confidentiality field.
    High,
    /// Low ("MC:L") value for the Modified Confidentiality field.
    Low,
    /// None ("MC:N") value for the Modified Confidentiality field.
    None,
}

/// The Modified Integrity field as defined by the CVSS specification.
#[derive(Debug, Clone, Copy, PartialEq, Deserialize, Serialize)]
pub enum ModifiedIntegrity {
    /// High ("MI:H") value for the Modified Integrity field.
    High,
    /// Low ("MI:L") value for the Modified Integrity field.
    Low,
    /// None ("MI:N") value for the Modified Integrity field.
    None,
}

/// The Modified Availability field as defined by the CVSS specification.
#[derive(Debug, Clone, Copy, PartialEq, Deserialize, Serialize)]
pub enum ModifiedAvailability {
    /// High ("MA:H") value for the Modified Availability field.
    High,
    /// Low ("MA:L") value for the Modified Availability field.
    Low,
    /// None ("MA:N") value for the Modified Availability field.
    None,
}

impl ModifiedImpact {
    #[doc(hidden)]
    pub fn score(
        original_impact: Impact,
        original_scope: Scope,
        modified_impact: Option<ModifiedImpact>,
        security_requirements: Option<SecurityRequirements>,
        modified_scope: Option<ModifiedScope>,
    ) -> f64 {
        let miss = ModifiedImpact::modified_impact_subscore(
            original_impact,
            modified_impact,
            security_requirements,
        );

        let scope_changed = match modified_scope {
            None => original_scope == Scope::Changed,
            Some(scope) => scope == ModifiedScope::Changed,
        };

        if scope_changed {
            7.52 * (miss - 0.029) - 3.25 * (miss - 0.02).powi(15)
        } else {
            6.42 * miss
        }
    }

    fn modified_impact_subscore(
        original_impact: Impact,
        modified_impact: Option<ModifiedImpact>,
        security_requirements: Option<SecurityRequirements>,
    ) -> f64 {
        let (confidentiality_requirement, integrity_requirement, availability_requirement) =
            match security_requirements {
                None => (1.0, 1.0, 1.0),
                Some(requirements) => (
                    match requirements.confidentiality_requirement {
                        None => 1.0,
                        Some(requirement) => requirement.numerical_value(),
                    },
                    match requirements.integrity_requirement {
                        None => 1.0,
                        Some(requirement) => requirement.numerical_value(),
                    },
                    match requirements.availability_requirement {
                        None => 1.0,
                        Some(requirement) => requirement.numerical_value(),
                    },
                ),
            };

        let (modified_confidentiality, modified_integrity, modified_availability) = {
            match modified_impact {
                None => (
                    original_impact.confidentiality.numerical_value(),
                    original_impact.integrity.numerical_value(),
                    original_impact.availability.numerical_value(),
                ),
                Some(impact) => (
                    match impact.modified_confidentiality {
                        Some(confidentiality) => confidentiality.numerical_value(),
                        None => original_impact.confidentiality.numerical_value(),
                    },
                    match impact.modified_integrity {
                        Some(integrity) => integrity.numerical_value(),
                        None => original_impact.integrity.numerical_value(),
                    },
                    match impact.modified_availability {
                        Some(availability) => availability.numerical_value(),
                        None => original_impact.availability.numerical_value(),
                    },
                ),
            }
        };

        f64::min(
            1.0 - (1.0 - confidentiality_requirement * modified_confidentiality)
                * (1.0 - integrity_requirement * modified_integrity)
                * (1.0 - availability_requirement * modified_availability),
            0.915,
        )
    }
}

impl ModifiedConfidentiality {
    /// Provides the numerical value associated with the metric, as specified by the CVSS specification.
    ///
    /// In normal usage you should not need to call this yourself.
    pub fn numerical_value(&self) -> f64 {
        match *self {
            ModifiedConfidentiality::High => 0.56,
            ModifiedConfidentiality::Low => 0.22,
            ModifiedConfidentiality::None => 0.0,
        }
    }
}

impl Field for ModifiedConfidentiality {
    fn abbreviated_form(&self) -> &'static str {
        match *self {
            ModifiedConfidentiality::High => "MC:H",
            ModifiedConfidentiality::Low => "MC:L",
            ModifiedConfidentiality::None => "MC:N",
        }
    }

    fn error_message() -> &'static str {
        // We do not define any error message for optional fields
        unreachable!()
    }

    fn parse(input: &str) -> Option<ModifiedConfidentiality> {
        if input == ModifiedConfidentiality::High.abbreviated_form() {
            Some(ModifiedConfidentiality::High)
        } else if input == ModifiedConfidentiality::Low.abbreviated_form() {
            Some(ModifiedConfidentiality::Low)
        } else if input == ModifiedConfidentiality::None.abbreviated_form() {
            Some(ModifiedConfidentiality::None)
        } else {
            None
        }
    }
}

impl ModifiedIntegrity {
    /// Provides the numerical value associated with the metric, as specified by the CVSS specification.
    ///
    /// In normal usage you should not need to call this yourself.
    pub fn numerical_value(&self) -> f64 {
        match *self {
            ModifiedIntegrity::High => 0.56,
            ModifiedIntegrity::Low => 0.22,
            ModifiedIntegrity::None => 0.0,
        }
    }
}

impl Field for ModifiedIntegrity {
    fn abbreviated_form(&self) -> &'static str {
        match *self {
            ModifiedIntegrity::High => "MI:H",
            ModifiedIntegrity::Low => "MI:L",
            ModifiedIntegrity::None => "MI:N",
        }
    }

    fn error_message() -> &'static str {
        // We do not define any error message for optional fields
        unreachable!()
    }

    fn parse(input: &str) -> Option<ModifiedIntegrity> {
        if input == ModifiedIntegrity::High.abbreviated_form() {
            Some(ModifiedIntegrity::High)
        } else if input == ModifiedIntegrity::Low.abbreviated_form() {
            Some(ModifiedIntegrity::Low)
        } else if input == ModifiedIntegrity::None.abbreviated_form() {
            Some(ModifiedIntegrity::None)
        } else {
            None
        }
    }
}

impl ModifiedAvailability {
    /// Provides the numerical value associated with the metric, as specified by the CVSS specification.
    ///
    /// In normal usage you should not need to call this yourself.
    pub fn numerical_value(&self) -> f64 {
        match *self {
            ModifiedAvailability::High => 0.56,
            ModifiedAvailability::Low => 0.22,
            ModifiedAvailability::None => 0.0,
        }
    }
}

impl Field for ModifiedAvailability {
    fn abbreviated_form(&self) -> &'static str {
        match *self {
            ModifiedAvailability::High => "MA:H",
            ModifiedAvailability::Low => "MA:L",
            ModifiedAvailability::None => "MA:N",
        }
    }

    fn error_message() -> &'static str {
        // We do not define any error message for optional fields
        unreachable!()
    }

    fn parse(input: &str) -> Option<ModifiedAvailability> {
        if input == ModifiedAvailability::High.abbreviated_form() {
            Some(ModifiedAvailability::High)
        } else if input == ModifiedAvailability::Low.abbreviated_form() {
            Some(ModifiedAvailability::Low)
        } else if input == ModifiedAvailability::None.abbreviated_form() {
            Some(ModifiedAvailability::None)
        } else {
            None
        }
    }
}

impl OptionalParser for ModifiedImpact {
    fn parse_optional(split: &Box<Split<char>>) -> Option<Self>
    where
        Self: Sized,
    {
        let res = ModifiedImpact {
            modified_confidentiality: ModifiedConfidentiality::parse_optional(split),
            modified_integrity: ModifiedIntegrity::parse_optional(split),
            modified_availability: ModifiedAvailability::parse_optional(split),
        };

        if res.modified_confidentiality.is_none()
            && res.modified_integrity.is_none()
            && res.modified_availability.is_none()
        {
            None
        } else {
            Some(res)
        }
    }
}

impl OptionalParser for ModifiedConfidentiality {
    fn parse_optional(split: &Box<Split<char>>) -> Option<Self>
    where
        Self: Sized,
    {
        parsing::parse_optional_default_field(split)
    }
}

impl OptionalParser for ModifiedIntegrity {
    fn parse_optional(split: &Box<Split<char>>) -> Option<Self>
    where
        Self: Sized,
    {
        parsing::parse_optional_default_field(split)
    }
}

impl OptionalParser for ModifiedAvailability {
    fn parse_optional(split: &Box<Split<char>>) -> Option<Self>
    where
        Self: Sized,
    {
        parsing::parse_optional_default_field(split)
    }
}

impl std::fmt::Display for ModifiedConfidentiality {
    fn fmt(&self, f: &mut Formatter) -> Result<(), Error> {
        write!(f, "{}", self.abbreviated_form())
    }
}

impl std::fmt::Display for ModifiedIntegrity {
    fn fmt(&self, f: &mut Formatter) -> Result<(), Error> {
        write!(f, "{}", self.abbreviated_form())
    }
}

impl std::fmt::Display for ModifiedAvailability {
    fn fmt(&self, f: &mut Formatter) -> Result<(), Error> {
        write!(f, "{}", self.abbreviated_form(),)
    }
}

impl std::fmt::Display for ModifiedImpact {
    fn fmt(&self, f: &mut Formatter) -> Result<(), Error> {
        // Fancy way to write a/b/c which degrade to a/b, b/c, a etc. when any field is missing (as they are all optional)
        let mut fields = Vec::new();

        match self.modified_confidentiality {
            None => (),
            Some(field) => fields.push(field.abbreviated_form()),
        };

        match self.modified_integrity {
            None => (),
            Some(field) => fields.push(field.abbreviated_form()),
        };

        match self.modified_availability {
            None => (),
            Some(field) => fields.push(field.abbreviated_form()),
        };

        write!(f, "{}", fields.join("/"))
    }
}

#[cfg(test)]
pub mod tests {
    use super::*;
    use crate::v3::base::impact::{Availability, Confidentiality, Integrity};
    use crate::v3::environmental::security_requirements::IntegrityRequirement;

    #[test]
    fn test_formatting() {
        // Confidentiality
        assert_eq!("MC:H", format!("{}", ModifiedConfidentiality::High));
        assert_eq!("MC:L", format!("{}", ModifiedConfidentiality::Low));
        assert_eq!("MC:N", format!("{}", ModifiedConfidentiality::None));

        // Integrity
        assert_eq!("MI:H", format!("{}", ModifiedIntegrity::High));
        assert_eq!("MI:L", format!("{}", ModifiedIntegrity::Low));
        assert_eq!("MI:N", format!("{}", ModifiedIntegrity::None));

        // Availability
        assert_eq!("MA:H", format!("{}", ModifiedAvailability::High));
        assert_eq!("MA:L", format!("{}", ModifiedAvailability::Low));
        assert_eq!("MA:N", format!("{}", ModifiedAvailability::None));

        // Impact vector
        assert_eq!(
            "MC:L/MI:H/MA:N",
            format!("{}", provide_modified_impact_vector1())
        );

        let mut missing_fields = provide_modified_impact_vector1();
        missing_fields.modified_integrity = None;
        assert_eq!("MC:L/MA:N", format!("{}", missing_fields));
    }

    #[test]
    fn test_parsing() {
        // Confidentiality
        assert_eq!(
            Some(ModifiedConfidentiality::High),
            ModifiedConfidentiality::parse("MC:H")
        );
        assert_eq!(
            Some(ModifiedConfidentiality::Low),
            ModifiedConfidentiality::parse("MC:L")
        );
        assert_eq!(
            Some(ModifiedConfidentiality::None),
            ModifiedConfidentiality::parse("MC:N")
        );
        assert_eq!(None, ModifiedConfidentiality::parse("fslfjskfj"));
        assert_eq!(None, ModifiedConfidentiality::parse("MC:"));
        assert_eq!(None, ModifiedConfidentiality::parse("MC:NA"));

        // Integrity
        assert_eq!(
            Some(ModifiedIntegrity::High),
            ModifiedIntegrity::parse("MI:H")
        );
        assert_eq!(
            Some(ModifiedIntegrity::Low),
            ModifiedIntegrity::parse("MI:L")
        );
        assert_eq!(
            Some(ModifiedIntegrity::None),
            ModifiedIntegrity::parse("MI:N")
        );
        assert_eq!(None, ModifiedIntegrity::parse("fslfjskfj"));
        assert_eq!(None, ModifiedIntegrity::parse("MI:"));
        assert_eq!(None, ModifiedIntegrity::parse("MI:NA"));

        // Availability
        assert_eq!(
            Some(ModifiedAvailability::High),
            ModifiedAvailability::parse("MA:H")
        );
        assert_eq!(
            Some(ModifiedAvailability::Low),
            ModifiedAvailability::parse("MA:L")
        );
        assert_eq!(
            Some(ModifiedAvailability::None),
            ModifiedAvailability::parse("MA:N")
        );
        assert_eq!(None, ModifiedAvailability::parse("fslfjskfj"));
        assert_eq!(None, ModifiedAvailability::parse("MA:"));
        assert_eq!(None, ModifiedAvailability::parse("MA:NA"));

        // Impact vector
        assert_eq!(
            Some(provide_modified_impact_vector1()),
            ModifiedImpact::parse_optional(&mut Box::new("MC:L/MI:H/MA:N".split('/'))),
        );

        assert_eq!(
            Some(provide_modified_impact_vector1()),
            ModifiedImpact::parse_optional(&mut Box::new("MI:H/MC:L/MA:N".split('/'))),
        );

        let mut missing_fields = provide_modified_impact_vector1();
        missing_fields.modified_integrity = None;
        assert_eq!(
            Some(missing_fields),
            ModifiedImpact::parse_optional(&mut Box::new("MC:L/MA:N".split('/'))),
        );

        assert_eq!(
            None,
            ModifiedImpact::parse_optional(&mut Box::new("fsjfskhf".split('/'))),
        );

        assert_eq!(
            None,
            ModifiedImpact::parse_optional(&mut Box::new("fs//jf/skhf".split('/'))),
        );
    }

    #[test]
    fn test_scoring() {
        let original_impact = Impact {
            confidentiality: Confidentiality::Low,
            integrity: Integrity::Low,
            availability: Availability::None,
        };

        let original_scope = Scope::Unchanged;

        let security_requirements = Some(SecurityRequirements {
            confidentiality_requirement: None,
            integrity_requirement: Some(IntegrityRequirement::High),
            availability_requirement: None,
        });

        let modified_scope = Some(ModifiedScope::Changed);

        let modified_impact = Some(ModifiedImpact {
            modified_confidentiality: None,
            modified_integrity: Some(ModifiedIntegrity::High),
            modified_availability: None,
        });

        // The result should be exactly 0.3916, but unfortunately there is some small IEEE floating point pollution here.
        // Therefore the test only validates the correctness up to 4 decimals.
        // Higher level unit tests and integration tests will prove that the final CVSS score is not impacted.
        assert_eq!(
            0.8752,
            (ModifiedImpact::modified_impact_subscore(
                original_impact,
                modified_impact,
                security_requirements
            ) * 10000.0)
                .round()
                / 10000.0
        );

        assert_eq!(
            6.052324907679383,
            ModifiedImpact::score(
                original_impact,
                original_scope,
                modified_impact,
                security_requirements,
                modified_scope
            )
        );
    }

    // This example is invented from scratch as the CVSS 3.0 specification does not provide a temporal vector example.
    pub fn provide_modified_impact_vector1() -> ModifiedImpact {
        ModifiedImpact {
            modified_confidentiality: Some(ModifiedConfidentiality::Low),
            modified_integrity: Some(ModifiedIntegrity::High),
            modified_availability: Some(ModifiedAvailability::None),
        }
    }
}
