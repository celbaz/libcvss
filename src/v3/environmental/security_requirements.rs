//! Security Requirements sub-vector implementation.

use crate::parsing;
use crate::parsing::{Field, OptionalParser};
use serde::{Deserialize, Serialize};
use std::fmt::{Error, Formatter};
use std::str::Split;

/// The Security Requirements sub-vector as defined by the CVSS specification.
#[derive(Debug, Clone, Copy, PartialEq, Deserialize, Serialize)]
pub struct SecurityRequirements {
    /// Optional Confidentiality Requirements field as defined by the CVSS specification.
    pub confidentiality_requirement: Option<ConfidentialityRequirement>,
    /// Optional Integrity Requirements field as defined by the CVSS specification.
    pub integrity_requirement: Option<IntegrityRequirement>,
    /// Optional Availability Requirements field as defined by the CVSS specification.
    pub availability_requirement: Option<AvailabilityRequirement>,
}

/// The Confidentiality Requirement field as defined by the CVSS specification.
///
/// "Not Defined" is considered the same as a missing value. This is done by setting [`SecurityRequirements.confidentiality_requirement`] to [`None`].
///
/// [`SecurityRequirements.confidentiality_requirement`]: struct.SecurityRequirements.html#structfield.confidentiality_requirement
/// [`None`]: https://doc.rust-lang.org/std/option/enum.Option.html#variant.None
#[derive(Debug, Clone, Copy, PartialEq, Deserialize, Serialize)]
pub enum ConfidentialityRequirement {
    // "Not Defined" is considered the same as a missing value. This is done by setting Temporal.exploit_code_maturity to None.
    /// High ("CR:H") value for the Confidentiality Requirement field.
    High,
    /// Medium ("CR:M") value for the Confidentiality Requirement field.
    Medium,
    /// Low ("CR:L") value for the Confidentiality Requirement field.
    Low,
}

/// The Integrity Requirement field as defined by the CVSS specification.
///
/// "Not Defined" is considered the same as a missing value. This is done by setting [`SecurityRequirements.integrity_requirement`] to [`None`].
///
/// [`SecurityRequirements.integrity_requirement`]: struct.SecurityRequirements.html#structfield.integrity_requirement
/// [`None`]: https://doc.rust-lang.org/std/option/enum.Option.html#variant.None
#[derive(Debug, Clone, Copy, PartialEq, Deserialize, Serialize)]
pub enum IntegrityRequirement {
    // "Not Defined" is considered the same as a missing value. This is done by setting Temporal.exploit_code_maturity to None.
    /// High ("IR:H") value for the Integrity Requirement field.
    High,
    /// Medium ("IR:M") value for the Integrity Requirement field.
    Medium,
    /// Low ("IR:L") value for the Integrity Requirement field.
    Low,
}

/// The Availability Requirement field as defined by the CVSS specification.
///
/// "Not Defined" is considered the same as a missing value. This is done by setting [`SecurityRequirements.availability_requirement`] to [`None`].
///
/// [`SecurityRequirements.availability_requirement`]: struct.SecurityRequirements.html#structfield.availability_requirement
/// [`None`]: https://doc.rust-lang.org/std/option/enum.Option.html#variant.None
#[derive(Debug, Clone, Copy, PartialEq, Deserialize, Serialize)]
pub enum AvailabilityRequirement {
    // "Not Defined" is considered the same as a missing value. This is done by setting Temporal.exploit_code_maturity to None.
    /// High ("AR:H") value for the Availability Requirement field.
    High,
    /// Medium ("AR:M") value for the Availability Requirement field.
    Medium,
    /// Low ("AR:L") value for the Availability Requirement field.
    Low,
}

impl ConfidentialityRequirement {
    /// Provides the numerical value associated with the metric, as specified by the CVSS specification.
    ///
    /// In normal usage you should not need to call this yourself.
    pub fn numerical_value(&self) -> f64 {
        match *self {
            ConfidentialityRequirement::High => 1.5,
            ConfidentialityRequirement::Medium => 1.0,
            ConfidentialityRequirement::Low => 0.5,
        }
    }
}

impl IntegrityRequirement {
    /// Provides the numerical value associated with the metric, as specified by the CVSS specification.
    ///
    /// In normal usage you should not need to call this yourself.
    pub fn numerical_value(&self) -> f64 {
        match *self {
            IntegrityRequirement::High => 1.5,
            IntegrityRequirement::Medium => 1.0,
            IntegrityRequirement::Low => 0.5,
        }
    }
}

impl AvailabilityRequirement {
    /// Provides the numerical value associated with the metric, as specified by the CVSS specification.
    ///
    /// In normal usage you should not need to call this yourself.
    pub fn numerical_value(&self) -> f64 {
        match *self {
            AvailabilityRequirement::High => 1.5,
            AvailabilityRequirement::Medium => 1.0,
            AvailabilityRequirement::Low => 0.5,
        }
    }
}

impl OptionalParser for SecurityRequirements {
    fn parse_optional(split: &Box<Split<char>>) -> Option<Self>
    where
        Self: Sized,
    {
        let res = SecurityRequirements {
            confidentiality_requirement: ConfidentialityRequirement::parse_optional(split),
            integrity_requirement: IntegrityRequirement::parse_optional(split),
            availability_requirement: AvailabilityRequirement::parse_optional(split),
        };

        if res.confidentiality_requirement.is_none()
            && res.integrity_requirement.is_none()
            && res.availability_requirement.is_none()
        {
            None
        } else {
            Some(res)
        }
    }
}

impl Field for ConfidentialityRequirement {
    fn abbreviated_form(&self) -> &'static str {
        match *self {
            ConfidentialityRequirement::High => "CR:H",
            ConfidentialityRequirement::Medium => "CR:M",
            ConfidentialityRequirement::Low => "CR:L",
        }
    }

    fn error_message() -> &'static str {
        // We do not define any error message for optional fields
        unreachable!()
    }

    fn parse(input: &str) -> Option<ConfidentialityRequirement> {
        if input == ConfidentialityRequirement::High.abbreviated_form() {
            Some(ConfidentialityRequirement::High)
        } else if input == ConfidentialityRequirement::Medium.abbreviated_form() {
            Some(ConfidentialityRequirement::Medium)
        } else if input == ConfidentialityRequirement::Low.abbreviated_form() {
            Some(ConfidentialityRequirement::Low)
        } else {
            None
        }
    }
}

impl Field for IntegrityRequirement {
    fn abbreviated_form(&self) -> &'static str {
        match *self {
            IntegrityRequirement::High => "IR:H",
            IntegrityRequirement::Medium => "IR:M",
            IntegrityRequirement::Low => "IR:L",
        }
    }

    fn error_message() -> &'static str {
        // We do not define any error message for optional fields
        unreachable!()
    }

    fn parse(input: &str) -> Option<IntegrityRequirement> {
        if input == IntegrityRequirement::High.abbreviated_form() {
            Some(IntegrityRequirement::High)
        } else if input == IntegrityRequirement::Medium.abbreviated_form() {
            Some(IntegrityRequirement::Medium)
        } else if input == IntegrityRequirement::Low.abbreviated_form() {
            Some(IntegrityRequirement::Low)
        } else {
            None
        }
    }
}

impl Field for AvailabilityRequirement {
    fn abbreviated_form(&self) -> &'static str {
        match *self {
            AvailabilityRequirement::High => "AR:H",
            AvailabilityRequirement::Medium => "AR:M",
            AvailabilityRequirement::Low => "AR:L",
        }
    }

    fn error_message() -> &'static str {
        // We do not define any error message for optional fields
        unreachable!()
    }

    fn parse(input: &str) -> Option<AvailabilityRequirement> {
        if input == AvailabilityRequirement::High.abbreviated_form() {
            Some(AvailabilityRequirement::High)
        } else if input == AvailabilityRequirement::Medium.abbreviated_form() {
            Some(AvailabilityRequirement::Medium)
        } else if input == AvailabilityRequirement::Low.abbreviated_form() {
            Some(AvailabilityRequirement::Low)
        } else {
            None
        }
    }
}

impl OptionalParser for ConfidentialityRequirement {
    fn parse_optional(split: &Box<Split<char>>) -> Option<Self>
    where
        Self: Sized,
    {
        parsing::parse_optional_default_field(split)
    }
}

impl OptionalParser for IntegrityRequirement {
    fn parse_optional(split: &Box<Split<char>>) -> Option<Self>
    where
        Self: Sized,
    {
        parsing::parse_optional_default_field(split)
    }
}

impl OptionalParser for AvailabilityRequirement {
    fn parse_optional(split: &Box<Split<char>>) -> Option<Self>
    where
        Self: Sized,
    {
        parsing::parse_optional_default_field(split)
    }
}

impl std::fmt::Display for ConfidentialityRequirement {
    fn fmt(&self, f: &mut Formatter) -> Result<(), Error> {
        write!(f, "{}", self.abbreviated_form())
    }
}

impl std::fmt::Display for IntegrityRequirement {
    fn fmt(&self, f: &mut Formatter) -> Result<(), Error> {
        write!(f, "{}", self.abbreviated_form())
    }
}

impl std::fmt::Display for AvailabilityRequirement {
    fn fmt(&self, f: &mut Formatter) -> Result<(), Error> {
        write!(f, "{}", self.abbreviated_form(),)
    }
}

impl std::fmt::Display for SecurityRequirements {
    fn fmt(&self, f: &mut Formatter) -> Result<(), Error> {
        // Fancy way to write a/b/c which degrade to a/b, b/c, a etc. when any field is missing (as they are all optional)
        let mut fields = Vec::new();

        match self.confidentiality_requirement {
            None => (),
            Some(field) => fields.push(field.abbreviated_form()),
        };

        match self.integrity_requirement {
            None => (),
            Some(field) => fields.push(field.abbreviated_form()),
        };

        match self.availability_requirement {
            None => (),
            Some(field) => fields.push(field.abbreviated_form()),
        };

        write!(f, "{}", fields.join("/"))
    }
}

#[cfg(test)]
pub mod tests {
    use super::*;

    #[test]
    fn test_formatting() {
        // Confidentiality
        assert_eq!("CR:H", format!("{}", ConfidentialityRequirement::High));
        assert_eq!("CR:M", format!("{}", ConfidentialityRequirement::Medium));
        assert_eq!("CR:L", format!("{}", ConfidentialityRequirement::Low));

        // Integrity
        assert_eq!("IR:H", format!("{}", IntegrityRequirement::High));
        assert_eq!("IR:M", format!("{}", IntegrityRequirement::Medium));
        assert_eq!("IR:L", format!("{}", IntegrityRequirement::Low));

        // Availability
        assert_eq!("AR:H", format!("{}", AvailabilityRequirement::High));
        assert_eq!("AR:M", format!("{}", AvailabilityRequirement::Medium));
        assert_eq!("AR:L", format!("{}", AvailabilityRequirement::Low));

        // Security requirements vector
        assert_eq!(
            "CR:H/IR:H/AR:L",
            format!("{}", provide_security_requirements_vector1())
        );

        // Missing fields
        let mut missing_fields = provide_security_requirements_vector1();
        missing_fields.integrity_requirement = None;
        assert_eq!("CR:H/AR:L", format!("{}", missing_fields));
    }

    #[test]
    fn test_parsing() {
        // Confidentiality
        assert_eq!(
            Some(ConfidentialityRequirement::High),
            ConfidentialityRequirement::parse("CR:H")
        );
        assert_eq!(
            Some(ConfidentialityRequirement::Low),
            ConfidentialityRequirement::parse("CR:L")
        );
        assert_eq!(
            Some(ConfidentialityRequirement::Medium),
            ConfidentialityRequirement::parse("CR:M")
        );
        assert_eq!(None, ConfidentialityRequirement::parse("fslfjskfj"));
        assert_eq!(None, ConfidentialityRequirement::parse("CR:"));
        assert_eq!(None, ConfidentialityRequirement::parse("CR:MA"));

        // Integrity
        assert_eq!(
            Some(IntegrityRequirement::High),
            IntegrityRequirement::parse("IR:H")
        );
        assert_eq!(
            Some(IntegrityRequirement::Low),
            IntegrityRequirement::parse("IR:L")
        );
        assert_eq!(
            Some(IntegrityRequirement::Medium),
            IntegrityRequirement::parse("IR:M")
        );
        assert_eq!(None, IntegrityRequirement::parse("fslfjskfj"));
        assert_eq!(None, IntegrityRequirement::parse("IR:"));
        assert_eq!(None, IntegrityRequirement::parse("IR:MA"));

        // Availability
        assert_eq!(
            Some(AvailabilityRequirement::High),
            AvailabilityRequirement::parse("AR:H")
        );
        assert_eq!(
            Some(AvailabilityRequirement::Low),
            AvailabilityRequirement::parse("AR:L")
        );
        assert_eq!(
            Some(AvailabilityRequirement::Medium),
            AvailabilityRequirement::parse("AR:M")
        );
        assert_eq!(None, AvailabilityRequirement::parse("fslfjskfj"));
        assert_eq!(None, AvailabilityRequirement::parse("AR:"));
        assert_eq!(None, AvailabilityRequirement::parse("AR:MA"));

        // Security requirements vector
        assert_eq!(
            Some(provide_security_requirements_vector1()),
            SecurityRequirements::parse_optional(&mut Box::new("CR:H/IR:H/AR:L".split('/'))),
        );

        assert_eq!(
            Some(provide_security_requirements_vector1()),
            SecurityRequirements::parse_optional(&mut Box::new("IR:H/CR:H/AR:L".split('/'))),
        );

        assert_eq!(
            None,
            SecurityRequirements::parse_optional(&mut Box::new("fsjfskhf".split('/'))),
        );
        assert_eq!(
            None,
            SecurityRequirements::parse_optional(&mut Box::new("fs//jf/skhf".split('/'))),
        );
    }

    // This example is invented from scratch as the CVSS 3.0 specification does not provide a temporal vector example.
    pub fn provide_security_requirements_vector1() -> SecurityRequirements {
        SecurityRequirements {
            confidentiality_requirement: Some(ConfidentialityRequirement::High),
            integrity_requirement: Some(IntegrityRequirement::High),
            availability_requirement: Some(AvailabilityRequirement::Low),
        }
    }
}
