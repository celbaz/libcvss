//! Roundup implementation.
//!
//! This is a roundup function implementation, as defined by CVSS 3.1 Specification - Appendix A.

/// Roundup returns the smallest number, specified to 1 decimal place, that is equal to or higher than its input.
///
/// For example, Roundup (4.02) returns 4.1; and Roundup (4.00) returns
/// 4.0. To ensure consistent results across programming languages and hardware, see CVSS 3.1 specification - Appendix
/// A for advice to Implementers on avoiding small inaccuracies introduced in some floating point
/// implementations.
pub fn roundup(input: f64) -> f64 {
    let int_input = (input * 100000.0).round() as i64;
    if int_input % 10000 == 0 {
        (int_input as f64 / 100000.0)
    } else {
        ((int_input / 10000) + 1) as f64 / 10.0
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_roundup() {
        assert_eq!(4.1, roundup(4.02));

        assert_eq!(4.00, roundup(4.0));

        assert_eq!(0.3, roundup(0.1 + 0.2));

        assert_eq!(0.3, roundup(0.3000001));

        assert_eq!(0.4, roundup(0.30001));
    }
}
