//! Base vector implementation.

use crate::parsing::Parser;
use crate::v31::base::exploitability::Exploitability;
use crate::v31::base::impact::Impact;
use crate::v31::base::scope::Scope;
use crate::v31::roundup::roundup;
use serde::{Deserialize, Serialize};
use std::fmt::{Error, Formatter};
use std::str::Split;

pub mod exploitability;
pub mod impact;
pub mod scope;

/// A CVSS V3.1 base vector.
///
/// Per the CVSS specification, this structure contains an Exploitability sub-vector, a Scope field, and an Impact sub-vector.
#[derive(Debug, Clone, Copy, PartialEq, Serialize, Deserialize)]
pub struct BaseVector {
    /// The Exploitability sub-vector as defined by the CVSS specification.
    pub exploitability: Exploitability,
    /// The Scope field as defined by the CVSS specification.
    pub scope: Scope,
    /// The Impact sub-vector as defined by the CVSS specification.
    pub impact: Impact,
}

impl BaseVector {
    /// Provides the severity score for the CVSS base vector.
    ///
    /// This score respects the CVSS 3.1 specification, particularly regarding floating-point roundup.
    ///
    /// Calling this method is identical to calling [`CVSS31Vector.score()`] when [`CVSS31Vector.temporal`] and [`CVSS31Vector.environmental`] are set to [`None`].
    ///
    /// [`CVSS31Vector.score()`]: ../struct.CVSS31Vector.html#method.score
    /// [`CVSS31Vector.temporal`]: ../struct.CVSS31Vector.html#structfield.temporal
    /// [`CVSS31Vector.environmental`]: ../struct.CVSS31Vector.html#structfield.environmental
    /// [`None`]: https://doc.rust-lang.org/std/option/enum.Option.html#variant.None
    pub fn score(&self) -> f64 {
        let impact_score = self.impact.score(self.scope);
        let exploitability_score = self.exploitability.score(self.scope);

        if impact_score > 0.0 {
            match self.scope {
                Scope::Unchanged => roundup(f64::min(impact_score + exploitability_score, 10.0)),
                Scope::Changed => {
                    roundup(f64::min(1.08 * (impact_score + exploitability_score), 10.0))
                }
            }
        } else {
            0.0
        }
    }

    #[doc(hidden)]
    pub fn cvss_prefix() -> &'static str {
        "CVSS:3.1"
    }

    fn parse_prefix(split: &mut Box<Split<char>>) -> Result<(), Vec<&'static str>> {
        static ERR_MSG: &str = "Header (CVSS3.1) is either missing or incorrect.";

        match split.next() {
            None => Err(vec![ERR_MSG]),
            Some(header) => {
                if header == BaseVector::cvss_prefix() {
                    Ok(())
                } else {
                    Err(vec![ERR_MSG])
                }
            }
        }
    }
}

impl Parser for BaseVector {
    fn parse_strict(split: &mut Box<Split<char>>) -> Result<Self, Vec<&'static str>>
    where
        Self: Sized,
    {
        let mut parsers = (None, None, None);
        let mut errors = Vec::new();

        let res = BaseVector::parse_prefix(split);
        if res.is_err() {
            errors.append(&mut res.unwrap_err());
        }

        let res = Exploitability::parse_strict(split);
        if res.is_ok() {
            parsers.0 = res.ok();
        } else {
            errors.append(&mut res.unwrap_err());
        }

        let res = Scope::parse_strict(split);
        if res.is_ok() {
            parsers.1 = res.ok();
        } else {
            errors.append(&mut res.unwrap_err());
        }

        let res = Impact::parse_strict(split);
        if res.is_ok() {
            parsers.2 = res.ok();
        } else {
            errors.append(&mut res.unwrap_err());
        }

        if parsers.0.is_some() && parsers.1.is_some() && parsers.2.is_some() && errors.len() == 0 {
            Ok(BaseVector {
                exploitability: parsers.0.unwrap(),
                scope: parsers.1.unwrap(),
                impact: parsers.2.unwrap(),
            })
        } else {
            Err(errors)
        }
    }

    fn parse_nonstrict(split: &Box<Split<char>>) -> Result<Self, Vec<&'static str>>
    where
        Self: Sized,
    {
        let mut parsers = (None, None, None);
        let mut errors = Vec::new();

        let res = Exploitability::parse_nonstrict(split);
        if res.is_ok() {
            parsers.0 = res.ok();
        } else {
            errors.append(&mut res.unwrap_err());
        }

        let res = Scope::parse_nonstrict(split);
        if res.is_ok() {
            parsers.1 = res.ok();
        } else {
            errors.append(&mut res.unwrap_err());
        }

        let res = Impact::parse_nonstrict(split);
        if res.is_ok() {
            parsers.2 = res.ok();
        } else {
            errors.append(&mut res.unwrap_err());
        }

        if parsers.0.is_some() && parsers.1.is_some() && parsers.2.is_some() {
            Ok(BaseVector {
                exploitability: parsers.0.unwrap(),
                scope: parsers.1.unwrap(),
                impact: parsers.2.unwrap(),
            })
        } else {
            Err(errors)
        }
    }
}

impl std::fmt::Display for BaseVector {
    fn fmt(&self, f: &mut Formatter) -> Result<(), Error> {
        write!(
            f,
            "{}/{}/{}/{}",
            BaseVector::cvss_prefix(),
            self.exploitability,
            self.scope,
            self.impact
        )
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_formatting() {
        assert_eq!(
            "CVSS:3.1/AV:N/AC:L/PR:H/UI:N/S:U/C:L/I:L/A:N",
            format!("{}", provide_base_vector1())
        );
    }

    #[test]
    fn test_parsing() {
        // Example provided in CVSS 3.1 specification section 6
        assert_eq!(
            Ok(provide_base_vector1()),
            BaseVector::parse_strict(&mut Box::new(
                "CVSS:3.1/AV:N/AC:L/PR:H/UI:N/S:U/C:L/I:L/A:N".split('/')
            )),
        );

        // Example provided in CVSS 3.1 specification section 6
        assert_eq!(
            Ok(provide_base_vector1()),
            BaseVector::parse_nonstrict(&mut Box::new(
                "CVSS:3.1/AV:N/AC:L/PR:H/UI:N/S:U/C:L/I:L/A:N".split('/')
            )),
        );

        // With two fields swapped inside a subvector (exploitability)
        assert!(BaseVector::parse_strict(&mut Box::new(
            "CVSS:3.1/AC:L/AV:N/PR:H/UI:N/S:U/C:L/I:L/A:N".split('/')
        ))
        .is_err());
        assert_eq!(
            Ok(provide_base_vector1()),
            BaseVector::parse_nonstrict(&mut Box::new(
                "CVSS:3.1/AC:L/AV:N/PR:H/UI:N/S:U/C:L/I:L/A:N".split('/')
            )),
        );

        // With two fields swapped between two subvector (exploitability and impact)
        assert!(BaseVector::parse_strict(&mut Box::new(
            "CVSS:3.1/AC:L/PR:H/UI:N/S:U/C:L/I:L/A:N/AV:N".split('/')
        ))
        .is_err());
        assert_eq!(
            Ok(provide_base_vector1()),
            BaseVector::parse_nonstrict(&mut Box::new(
                "CVSS:3.1/AC:L/PR:H/UI:N/S:U/C:L/I:L/A:N/AV:N".split('/')
            )),
        );

        // Example provided in CVSS 3.1 specification section 6
        assert!(BaseVector::parse_strict(&mut Box::new(
            "CVSS:3.1/S:U/AV:N/AC:L/PR:H/UI:N/C:L/I:L/A:N/E:F/RL:X".split('/')
        ))
        .is_err());
        assert_eq!(
            Ok(provide_base_vector1()),
            BaseVector::parse_nonstrict(&mut Box::new(
                "CVSS:3.1/S:U/AV:N/AC:L/PR:H/UI:N/C:L/I:L/A:N/E:F/RL:X".split('/')
            )),
        );

        // Sane example with prefix removed
        assert!(BaseVector::parse_strict(&mut Box::new(
            "AV:N/AC:L/PR:H/UI:N/S:U/C:L/I:L/A:N".split('/')
        ))
        .is_err());
        assert_eq!(
            Ok(provide_base_vector1()),
            BaseVector::parse_nonstrict(&mut Box::new(
                "AV:N/AC:L/PR:H/UI:N/S:U/C:L/I:L/A:N".split('/')
            )),
        );

        // Junk entry
        assert!(BaseVector::parse_strict(&mut Box::new("fsjfskhf".split('/'))).is_err());
        assert!(BaseVector::parse_nonstrict(&mut Box::new("fsjfskhf".split('/'))).is_err());

        assert!(BaseVector::parse_strict(&mut Box::new("fs//jf/skhf".split('/'))).is_err());
        assert!(BaseVector::parse_nonstrict(&mut Box::new("fs//jf/skhf".split('/'))).is_err());
    }

    #[test]
    fn test_scoring() {
        assert_eq!(3.8, provide_base_vector1().score());
    }

    // This example is taken from CVSS 3.1 specification section 6.
    fn provide_base_vector1() -> BaseVector {
        BaseVector {
            exploitability: exploitability::tests::provide_exploitability_vector1(),
            scope: Scope::Unchanged,
            impact: impact::tests::provide_impact_vector1(),
        }
    }
}
