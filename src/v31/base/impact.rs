//! Impact sub-vector implementation.

use crate::parsing;
use crate::parsing::{Field, Parser};
use crate::v31::base::scope::Scope;
use serde::{Deserialize, Serialize};
use std::fmt::{Error, Formatter};
use std::str::Split;

/// The Impact sub-vector as defined by the CVSS specification.
#[derive(Debug, Clone, Copy, PartialEq, Serialize, Deserialize)]
pub struct Impact {
    /// The Confidentiality field as defined by the CVSS specification.
    pub confidentiality: Confidentiality,
    /// The Integrity field as defined by the CVSS specification.
    pub integrity: Integrity,
    /// The Availability field as defined by the CVSS specification.
    pub availability: Availability,
}

/// The Confidentiality field as defined by the CVSS specification.
#[derive(Debug, Clone, Copy, PartialEq, Serialize, Deserialize)]
pub enum Confidentiality {
    /// High ("C:H") value for the Confidentiality field.
    High,
    /// Low ("C:L") value for the Confidentiality field.
    Low,
    /// None ("C:N") value for the Confidentiality field.
    None,
}

/// The Integrity field as defined by the CVSS specification.
#[derive(Debug, Clone, Copy, PartialEq, Serialize, Deserialize)]
pub enum Integrity {
    /// High ("I:H") value for the Integrity field.
    High,
    /// Low ("I:L") value for the Integrity field.
    Low,
    /// None ("I:N") value for the Integrity field.
    None,
}

/// The Availability field as defined by the CVSS specification.
#[derive(Debug, Clone, Copy, PartialEq, Serialize, Deserialize)]
pub enum Availability {
    /// High ("A:H") value for the Availability field.
    High,
    /// Low ("A:L") value for the Availability field.
    Low,
    /// None ("A:N") value for the Availability field.
    None,
}

impl Impact {
    #[doc(hidden)]
    pub fn score(&self, scope: Scope) -> f64 {
        match scope {
            Scope::Unchanged => 6.42 * self.impact_subscore(),
            Scope::Changed => {
                7.52 * (self.impact_subscore() - 0.029)
                    - 3.25 * (self.impact_subscore() - 0.02).powi(15)
            }
        }
    }

    fn impact_subscore(&self) -> f64 {
        1.0 - ((1.0 - self.confidentiality.numerical_value())
            * (1.0 - self.integrity.numerical_value())
            * (1.0 - self.availability.numerical_value()))
    }
}

impl Parser for Impact {
    fn parse_strict(split: &mut Box<Split<char>>) -> Result<Self, Vec<&'static str>>
    where
        Self: Sized,
    {
        let mut parsers = (None, None, None);
        let mut errors = Vec::new();

        let res = Confidentiality::parse_strict(split);
        if res.is_ok() {
            parsers.0 = res.ok();
        } else {
            errors.append(&mut res.unwrap_err());
        }

        let res = Integrity::parse_strict(split);
        if res.is_ok() {
            parsers.1 = res.ok();
        } else {
            errors.append(&mut res.unwrap_err());
        }

        let res = Availability::parse_strict(split);
        if res.is_ok() {
            parsers.2 = res.ok();
        } else {
            errors.append(&mut res.unwrap_err());
        }

        if parsers.0.is_some() && parsers.1.is_some() && parsers.2.is_some() {
            Ok(Impact {
                confidentiality: parsers.0.unwrap(),
                integrity: parsers.1.unwrap(),
                availability: parsers.2.unwrap(),
            })
        } else {
            Err(errors)
        }
    }

    fn parse_nonstrict(split: &Box<Split<char>>) -> Result<Self, Vec<&'static str>>
    where
        Self: Sized,
    {
        let mut parsers = (None, None, None);
        let mut errors = Vec::new();

        let res = Confidentiality::parse_nonstrict(split);
        if res.is_ok() {
            parsers.0 = res.ok();
        } else {
            errors.append(&mut res.unwrap_err());
        }

        let res = Integrity::parse_nonstrict(split);
        if res.is_ok() {
            parsers.1 = res.ok();
        } else {
            errors.append(&mut res.unwrap_err());
        }

        let res = Availability::parse_nonstrict(split);
        if res.is_ok() {
            parsers.2 = res.ok();
        } else {
            errors.append(&mut res.unwrap_err());
        }

        if parsers.0.is_some() && parsers.1.is_some() && parsers.2.is_some() {
            Ok(Impact {
                confidentiality: parsers.0.unwrap(),
                integrity: parsers.1.unwrap(),
                availability: parsers.2.unwrap(),
            })
        } else {
            Err(errors)
        }
    }
}

impl Confidentiality {
    /// Provides the numerical value associated with the metric, as specified by the CVSS specification.
    ///
    /// In normal usage you should not need to call this yourself.
    pub fn numerical_value(&self) -> f64 {
        match *self {
            Confidentiality::High => 0.56,
            Confidentiality::Low => 0.22,
            Confidentiality::None => 0.0,
        }
    }
}

impl Field for Confidentiality {
    fn abbreviated_form(&self) -> &'static str {
        match *self {
            Confidentiality::High => "C:H",
            Confidentiality::Low => "C:L",
            Confidentiality::None => "C:N",
        }
    }

    fn error_message() -> &'static str {
        "Confidentiality (C) could not be found."
    }

    fn parse(input: &str) -> Option<Confidentiality> {
        if input == Confidentiality::High.abbreviated_form() {
            Some(Confidentiality::High)
        } else if input == Confidentiality::Low.abbreviated_form() {
            Some(Confidentiality::Low)
        } else if input == Confidentiality::None.abbreviated_form() {
            Some(Confidentiality::None)
        } else {
            None
        }
    }
}

impl Parser for Confidentiality {
    fn parse_strict(split: &mut Box<Split<char>>) -> Result<Self, Vec<&'static str>>
    where
        Self: Sized,
    {
        parsing::parse_strict_default_field(split)
    }

    fn parse_nonstrict(split: &Box<Split<char>>) -> Result<Self, Vec<&'static str>>
    where
        Self: Sized,
    {
        parsing::parse_nonstrict_default_field(split)
    }
}

impl Integrity {
    /// Provides the numerical value associated with the metric, as specified by the CVSS specification.
    ///
    /// In normal usage you should not need to call this yourself.
    pub fn numerical_value(&self) -> f64 {
        match *self {
            Integrity::High => 0.56,
            Integrity::Low => 0.22,
            Integrity::None => 0.0,
        }
    }
}

impl Field for Integrity {
    fn abbreviated_form(&self) -> &'static str {
        match *self {
            Integrity::High => "I:H",
            Integrity::Low => "I:L",
            Integrity::None => "I:N",
        }
    }

    fn error_message() -> &'static str {
        "Integrity (I) could not be found."
    }

    fn parse(input: &str) -> Option<Integrity> {
        if input == Integrity::High.abbreviated_form() {
            Some(Integrity::High)
        } else if input == Integrity::Low.abbreviated_form() {
            Some(Integrity::Low)
        } else if input == Integrity::None.abbreviated_form() {
            Some(Integrity::None)
        } else {
            None
        }
    }
}

impl Parser for Integrity {
    fn parse_strict(split: &mut Box<Split<char>>) -> Result<Self, Vec<&'static str>>
    where
        Self: Sized,
    {
        parsing::parse_strict_default_field(split)
    }

    fn parse_nonstrict(split: &Box<Split<char>>) -> Result<Self, Vec<&'static str>>
    where
        Self: Sized,
    {
        parsing::parse_nonstrict_default_field(split)
    }
}

impl Availability {
    /// Provides the numerical value associated with the metric, as specified by the CVSS specification.
    ///
    /// In normal usage you should not need to call this yourself.
    pub fn numerical_value(&self) -> f64 {
        match *self {
            Availability::High => 0.56,
            Availability::Low => 0.22,
            Availability::None => 0.0,
        }
    }
}

impl Field for Availability {
    fn abbreviated_form(&self) -> &'static str {
        match *self {
            Availability::High => "A:H",
            Availability::Low => "A:L",
            Availability::None => "A:N",
        }
    }

    fn error_message() -> &'static str {
        "Availability (A) could not be found."
    }

    fn parse(input: &str) -> Option<Availability> {
        if input == Availability::High.abbreviated_form() {
            Some(Availability::High)
        } else if input == Availability::Low.abbreviated_form() {
            Some(Availability::Low)
        } else if input == Availability::None.abbreviated_form() {
            Some(Availability::None)
        } else {
            None
        }
    }
}

impl Parser for Availability {
    fn parse_strict(split: &mut Box<Split<char>>) -> Result<Self, Vec<&'static str>>
    where
        Self: Sized,
    {
        parsing::parse_strict_default_field(split)
    }

    fn parse_nonstrict(split: &Box<Split<char>>) -> Result<Self, Vec<&'static str>>
    where
        Self: Sized,
    {
        parsing::parse_nonstrict_default_field(split)
    }
}

impl std::fmt::Display for Confidentiality {
    fn fmt(&self, f: &mut Formatter) -> Result<(), Error> {
        write!(f, "{}", self.abbreviated_form())
    }
}

impl std::fmt::Display for Integrity {
    fn fmt(&self, f: &mut Formatter) -> Result<(), Error> {
        write!(f, "{}", self.abbreviated_form())
    }
}

impl std::fmt::Display for Availability {
    fn fmt(&self, f: &mut Formatter) -> Result<(), Error> {
        write!(f, "{}", self.abbreviated_form(),)
    }
}

impl std::fmt::Display for Impact {
    fn fmt(&self, f: &mut Formatter) -> Result<(), Error> {
        write!(
            f,
            "{}/{}/{}",
            self.confidentiality, self.integrity, self.availability,
        )
    }
}

#[cfg(test)]
pub mod tests {
    use super::*;

    #[test]
    fn test_formatting() {
        // Confidentiality
        assert_eq!("C:H", format!("{}", Confidentiality::High));
        assert_eq!("C:L", format!("{}", Confidentiality::Low));
        assert_eq!("C:N", format!("{}", Confidentiality::None));

        // Integrity
        assert_eq!("I:H", format!("{}", Integrity::High));
        assert_eq!("I:L", format!("{}", Integrity::Low));
        assert_eq!("I:N", format!("{}", Integrity::None));

        // Availability
        assert_eq!("A:H", format!("{}", Availability::High));
        assert_eq!("A:L", format!("{}", Availability::Low));
        assert_eq!("A:N", format!("{}", Availability::None));

        // Impact vector
        assert_eq!("C:L/I:L/A:N", format!("{}", provide_impact_vector1()));
    }

    #[test]
    fn test_parsing() {
        // Confidentiality
        assert_eq!(Some(Confidentiality::High), Confidentiality::parse("C:H"));
        assert_eq!(Some(Confidentiality::Low), Confidentiality::parse("C:L"));
        assert_eq!(Some(Confidentiality::None), Confidentiality::parse("C:N"));
        assert_eq!(None, Confidentiality::parse("fslfjskfj"));
        assert_eq!(None, Confidentiality::parse("C:"));
        assert_eq!(None, Confidentiality::parse("C:NA"));

        // Integrity
        assert_eq!(Some(Integrity::High), Integrity::parse("I:H"));
        assert_eq!(Some(Integrity::Low), Integrity::parse("I:L"));
        assert_eq!(Some(Integrity::None), Integrity::parse("I:N"));
        assert_eq!(None, Integrity::parse("fslfjskfj"));
        assert_eq!(None, Integrity::parse("I:"));
        assert_eq!(None, Integrity::parse("I:NA"));

        // Availability
        assert_eq!(Some(Availability::High), Availability::parse("A:H"));
        assert_eq!(Some(Availability::Low), Availability::parse("A:L"));
        assert_eq!(Some(Availability::None), Availability::parse("A:N"));
        assert_eq!(None, Availability::parse("fslfjskfj"));
        assert_eq!(None, Availability::parse("A:"));
        assert_eq!(None, Availability::parse("A:NA"));

        // Impact vector
        assert_eq!(
            Ok(provide_impact_vector1()),
            Impact::parse_strict(&mut Box::new("C:L/I:L/A:N".split('/'))),
        );

        assert_eq!(
            Ok(provide_impact_vector1()),
            Impact::parse_nonstrict(&mut Box::new("C:L/I:L/A:N".split('/'))),
        );

        assert!(Impact::parse_strict(&mut Box::new("I:L/C:L/A:N".split('/'))).is_err());
        assert_eq!(
            Ok(provide_impact_vector1()),
            Impact::parse_nonstrict(&mut Box::new("I:L/C:L/A:N".split('/'))),
        );

        assert!(Impact::parse_strict(&mut Box::new("fsjfskhf".split('/'))).is_err());
        assert!(Impact::parse_nonstrict(&mut Box::new("fsjfskhf".split('/'))).is_err());

        assert!(Impact::parse_strict(&mut Box::new("fs//jf/skhf".split('/'))).is_err());
        assert!(Impact::parse_nonstrict(&mut Box::new("fs//jf/skhf".split('/'))).is_err());
    }

    #[test]
    fn test_scoring() {
        // The result should be exactly 0.3916, but unfortunately there is some small IEEE floating point pollution here.
        // Therefore the test only validates the correctness up to 4 decimals.
        // Higher level unit tests and integration tests will prove that the final CVSS score is not impacted.
        assert_eq!(
            0.3916,
            (provide_impact_vector1().impact_subscore() * 10000.0).round() / 10000.0
        );

        assert_eq!(
            2.514072,
            (provide_impact_vector1().score(Scope::Unchanged) * 1000000.0).round() / 1000000.0
        );

        assert_eq!(
            2.726750844,
            (provide_impact_vector1().score(Scope::Changed) * 1000000000.0).round() / 1000000000.0
        );
    }

    // This example is taken from CVSS 3.1 specification section 6.
    pub fn provide_impact_vector1() -> Impact {
        Impact {
            confidentiality: Confidentiality::Low,
            integrity: Integrity::Low,
            availability: Availability::None,
        }
    }
}
