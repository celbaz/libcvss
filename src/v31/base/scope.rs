//! Scope field implementation.

use crate::parsing;
use crate::parsing::{Field, Parser};
use serde::{Deserialize, Serialize};
use std::fmt::{Error, Formatter};
use std::str::Split;

/// The Scope field as defined by the CVSS specification.
#[derive(Debug, Clone, Copy, PartialEq, Serialize, Deserialize)]
pub enum Scope {
    /// Unchanged ("S:U") value for the Scope field.
    Unchanged,
    /// Changed ("S:C") value for the Scope field.
    Changed,
}

impl Field for Scope {
    fn abbreviated_form(&self) -> &'static str {
        match *self {
            Scope::Unchanged => "S:U",
            Scope::Changed => "S:C",
        }
    }

    fn error_message() -> &'static str {
        "Scope (S) could not be found."
    }

    fn parse(input: &str) -> Option<Scope> {
        if input == Scope::Unchanged.abbreviated_form() {
            Some(Scope::Unchanged)
        } else if input == Scope::Changed.abbreviated_form() {
            Some(Scope::Changed)
        } else {
            None
        }
    }
}

impl Parser for Scope {
    fn parse_strict(split: &mut Box<Split<char>>) -> Result<Self, Vec<&'static str>>
    where
        Self: Sized,
    {
        parsing::parse_strict_default_field(split)
    }

    fn parse_nonstrict(split: &Box<Split<char>>) -> Result<Self, Vec<&'static str>>
    where
        Self: Sized,
    {
        parsing::parse_nonstrict_default_field(split)
    }
}

impl std::fmt::Display for Scope {
    fn fmt(&self, f: &mut Formatter) -> Result<(), Error> {
        write!(f, "{}", self.abbreviated_form())
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_formatting() {
        assert_eq!("S:U", format!("{}", Scope::Unchanged));
        assert_eq!("S:C", format!("{}", Scope::Changed));
    }

    #[test]
    fn test_parsing() {
        assert_eq!(Some(Scope::Unchanged), Scope::parse("S:U"));
        assert_eq!(Some(Scope::Changed), Scope::parse("S:C"));

        assert_eq!(None, Scope::parse("fsfdjslkdfjs"));
        assert_eq!(None, Scope::parse("S:UC"));
    }
}
