//! Environmental vector implementation.

use crate::parsing::OptionalParser;
use crate::v31::base::BaseVector;
use crate::v31::environmental::modified_base::ModifiedBaseVector;
use crate::v31::environmental::security_requirements::SecurityRequirements;
use crate::v31::temporal::TemporalVector;
use serde::{Deserialize, Serialize};
use std::fmt::{Error, Formatter};
use std::str::Split;

pub mod modified_base;
pub mod security_requirements;

/// A CVSS V3.1 environmental vector.
///
/// Per the CVSS specification, this structure contains a Security Requirements sub-vector and a Modified Base sub-vector, both optionals.
#[derive(Debug, Clone, Copy, PartialEq, Deserialize, Serialize)]
pub struct EnvironmentalVector {
    /// Optional Security Requirements sub-vector as defined by the CVSS specification.
    pub security_requirements: Option<SecurityRequirements>,
    /// Optional Modified Base sub-vector as defined by the CVSS specification.
    pub modified_base: Option<ModifiedBaseVector>,
}

impl EnvironmentalVector {
    /// Provides the severity score for the CVSS environmental vector, given a mandatory base vector and an optional environmental vector.
    ///
    /// This score respects the CVSS 3.1 specification, particularly regarding floating-point roundup.
    ///
    /// Calling this method is identical to calling [`CVSS31Vector.score()`].
    ///
    /// [`CVSS31Vector.score()`]: ../struct.CVSS31Vector.html#method.score
    pub fn score(&self, base_vector: BaseVector, temporal_vector: Option<TemporalVector>) -> f64 {
        ModifiedBaseVector::score(
            base_vector,
            temporal_vector,
            self.modified_base,
            self.security_requirements,
        )
    }
}

impl OptionalParser for EnvironmentalVector {
    fn parse_optional(split: &Box<Split<char>>) -> Option<Self>
    where
        Self: Sized,
    {
        let res = EnvironmentalVector {
            security_requirements: SecurityRequirements::parse_optional(split),
            modified_base: ModifiedBaseVector::parse_optional(split),
        };

        if res.security_requirements.is_none() && res.modified_base.is_none() {
            None
        } else {
            Some(res)
        }
    }
}

impl std::fmt::Display for EnvironmentalVector {
    fn fmt(&self, f: &mut Formatter) -> Result<(), Error> {
        // Fancy way to write a/b which degrade to a or b, a etc. when any field is missing (as they are all optional)
        let mut fields = Vec::new();

        match self.security_requirements {
            None => (),
            Some(field) => fields.push(format!("{}", field)),
        };

        match self.modified_base {
            None => (),
            Some(field) => fields.push(format!("{}", field)),
        };

        write!(f, "{}", fields.join("/"))
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::v31::base::exploitability::{
        AttackComplexity, AttackVector, Exploitability, PrivilegesRequired, UserInteraction,
    };
    use crate::v31::base::impact::{Availability, Confidentiality, Impact, Integrity};
    use crate::v31::base::scope::Scope;
    use crate::v31::environmental::modified_base::modified_exploitability::{
        ModifiedAttackComplexity, ModifiedAttackVector, ModifiedExploitability,
    };
    use crate::v31::environmental::security_requirements::{
        AvailabilityRequirement, ConfidentialityRequirement, IntegrityRequirement,
    };
    use crate::v31::temporal::{ExploitCodeMaturity, RemediationLevel, ReportConfidence};

    #[test]
    fn test_formatting() {
        assert_eq!(
            "CR:H/IR:H/AR:M/MAV:N/MAC:L",
            format!("{}", provide_environmental_vector())
        );
    }

    #[test]
    fn test_parsing() {
        // This example is invented from scratch as the CVSS 3.1 specification does not provide a environmental vector example.
        assert_eq!(
            Some(provide_environmental_vector()),
            EnvironmentalVector::parse_optional(&mut Box::new(
                "CR:H/IR:H/AR:M/MAV:N/MAC:L".split('/')
            ))
        );

        // With two fields swapped inside a subvector (security requirements)
        assert_eq!(
            Some(provide_environmental_vector()),
            EnvironmentalVector::parse_optional(&mut Box::new(
                "IR:H/CR:H/AR:M/MAV:N/MAC:L".split('/')
            ))
        );

        // With two fields swapped between two subvector (security requirements and modified base)
        assert_eq!(
            Some(provide_environmental_vector()),
            EnvironmentalVector::parse_optional(&mut Box::new(
                "IR:H/AR:M/MAV:N/CR:H/MAC:L".split('/')
            ))
        );

        // Junk entry
        assert_eq!(
            None,
            EnvironmentalVector::parse_optional(&mut Box::new("fsjfskhf".split('/')))
        );
        assert_eq!(
            None,
            EnvironmentalVector::parse_optional(&mut Box::new("fs//jf/skhf".split('/')))
        );
    }

    #[test]
    fn test_scoring() {
        let environmental_vector = provide_environmental_vector();

        assert_eq!(
            9.1,
            environmental_vector.score(provide_base_vector(), Some(provide_temporal_vector()))
        );
    }

    fn provide_base_vector() -> BaseVector {
        BaseVector {
            exploitability: Exploitability {
                attack_vector: AttackVector::Adjacent,
                attack_complexity: AttackComplexity::High,
                privileges_required: PrivilegesRequired::None,
                user_interaction: UserInteraction::None,
            },
            scope: Scope::Unchanged,
            impact: Impact {
                confidentiality: Confidentiality::High,
                integrity: Integrity::High,
                availability: Availability::High,
            },
        }
    }

    fn provide_temporal_vector() -> TemporalVector {
        TemporalVector {
            exploit_code_maturity: Some(ExploitCodeMaturity::Functional),
            remediation_level: Some(RemediationLevel::OfficialFix),
            report_confidence: Some(ReportConfidence::Confirmed),
        }
    }

    fn provide_environmental_vector() -> EnvironmentalVector {
        let modified_base_vector = ModifiedBaseVector {
            modified_exploitability: Some(ModifiedExploitability {
                modified_attack_vector: Some(ModifiedAttackVector::Network),
                modified_attack_complexity: Some(ModifiedAttackComplexity::Low),
                modified_privileges_required: None,
                modified_user_interaction: None,
            }),
            modified_scope: None,
            modified_impact: None,
        };

        let security_requirements = SecurityRequirements {
            confidentiality_requirement: Some(ConfidentialityRequirement::High),
            integrity_requirement: Some(IntegrityRequirement::High),
            availability_requirement: Some(AvailabilityRequirement::Medium),
        };

        EnvironmentalVector {
            security_requirements: Some(security_requirements),
            modified_base: Some(modified_base_vector),
        }
    }
}
