//! # What is libcvss?
//! libcvss is a Rust implementation of the CVSS specification.
//!
//! It provides Rust users with a native way to manipulate CVSS-formatted vulnerability data.
//!
//! libcvss leverages Rust to provide a CVSS implementation focused on both performance and correctness.
//! # What is CVSS?
//! [The official CVSS website](https://www.first.org/cvss/) describes CVSS this way:
//! > The Common Vulnerability Scoring System (CVSS) provides a way to capture the principal characteristics of a vulnerability and produce a numerical score reflecting its severity. The numerical score can then be translated into a qualitative representation (such as low, medium, high, and critical) to help organizations properly assess and prioritize their vulnerability management processes.
//! # Features
//! - All non-deprecated versions of CVSS ([2.0](v2/index.html), [3.0](v3/index.html), [3.1](v31/index.html)) are supported.
//! - CVSS vectors can be parsed from a string slice, or constructed programmatically.
//! - Parsing can be done in strict or non-strict mode: strict mode requires all fields inside the vector string to be present in canonical order. In non-strict mode the parser will do its best to construct a CVSS vector from what it can find in the input: fields can be repeated, in the wrong order, etc.
//! - Score associated with a CVSS vector can be computed for base, temporal or environmental vectors.
//! - libcvss can parse thousands of CVSS vectors per second.
//! # Test infrastructure and reliability
//! Correctness and reliability are an important focus of libcvss.
//! - All official examples from all versions of the CVSS specification are implemented as integration tests. We pledge to have these tests pass on master at all time.
//! - An optional integration test downloads the NVD data for every CVE vulnerability disclosed since 2007, parses the associated CVSS vectors (in both V2 and V3 version when available), computes the associated severity scores and compares it to the scores provided by NVD. We pledge to have this test pass on all libcvss releases.
//! # Examples
//! Create a [CVSS 3.1](v31/index.html) vector either programmatically or from a vector string, then compute its score:
//! ```
//! # use libcvss::v31::CVSS31Vector;
//! # use libcvss::v31::base::BaseVector;
//! # use libcvss::v31::base::exploitability::{Exploitability, AttackVector, AttackComplexity, PrivilegesRequired, UserInteraction};
//! # use libcvss::v31::base::scope::Scope;
//! # use libcvss::v31::base::impact::{Impact, Confidentiality, Integrity, Availability};
//! let cvss_vector_as_str = "CVSS:3.1/AV:N/AC:L/PR:L/UI:N/S:C/C:L/I:L/A:N";
//! let cvss_vector = CVSS31Vector {
//!            base: BaseVector {
//!                exploitability: Exploitability {
//!                    attack_vector: AttackVector::Network,
//!                    attack_complexity: AttackComplexity::Low,
//!                    privileges_required: PrivilegesRequired::Low,
//!                    user_interaction: UserInteraction::None,
//!                },
//!                scope: Scope::Changed,
//!                impact: Impact {
//!                    confidentiality: Confidentiality::Low,
//!                    integrity: Integrity::Low,
//!                    availability: Availability::None,
//!                },
//!            },
//!           temporal: None,
//!           environmental: None,
//!        };
//! assert_eq!(Ok(cvss_vector), CVSS31Vector::parse_strict(cvss_vector_as_str));
//! assert_eq!(cvss_vector_as_str, format!("{}", cvss_vector));
//! assert_eq!(6.4, cvss_vector.score());
//! ```
//! A similar example using [CVSS 3.0](v30/index.html):
//! ```
//! # use libcvss::v3::CVSS3Vector;
//! # use libcvss::v3::base::BaseVector;
//! # use libcvss::v3::base::exploitability::{Exploitability, AttackVector, AttackComplexity, PrivilegesRequired, UserInteraction};
//! # use libcvss::v3::base::scope::Scope;
//! # use libcvss::v3::base::impact::{Impact, Confidentiality, Integrity, Availability};
//! let cvss_vector_as_str = "CVSS:3.0/AV:N/AC:L/PR:N/UI:R/S:C/C:L/I:L/A:N";
//! let cvss_vector = CVSS3Vector {
//!           base: BaseVector {
//!               exploitability: Exploitability {
//!                   attack_vector: AttackVector::Network,
//!                   attack_complexity: AttackComplexity::Low,
//!                   privileges_required: PrivilegesRequired::None,
//!                   user_interaction: UserInteraction::Required,
//!               },
//!               scope: Scope::Changed,
//!               impact: Impact {
//!                   confidentiality: Confidentiality::Low,
//!                   integrity: Integrity::Low,
//!                   availability: Availability::None,
//!               },
//!           },
//!           temporal: None,
//!           environmental: None,
//!       };
//! assert_eq!(Ok(cvss_vector), CVSS3Vector::parse_strict(cvss_vector_as_str));
//! assert_eq!(cvss_vector_as_str, format!("{}", cvss_vector));
//! assert_eq!(6.1, cvss_vector.score());
//! ```
//! A similar example using [CVSS 2.0](v2/index.html), with additional temporal and environmental vectors:
//! ```
//! # use libcvss::v2::CVSS2Vector;
//! # use libcvss::v2::base::{BaseVector, AccessVector, AccessComplexity, Authentication, ConfidentialityImpact, IntegrityImpact, AvailabilityImpact};
//! # use libcvss::v2::temporal::{TemporalVector, Exploitability, RemediationLevel, ReportConfidence};
//! # use libcvss::v2::environmental::{EnvironmentalVector, CollateralDamagePotential, TargetDistribution, ConfidentialityRequirement, IntegrityRequirement, AvailabilityRequirement};
//! let cvss_vector_as_str = "AV:N/AC:L/Au:N/C:N/I:N/A:C/E:F/RL:OF/RC:C/CDP:H/TD:H/CR:M/IR:M/AR:H";
//! let cvss_vector = CVSS2Vector {
//!            base: BaseVector {
//!               access_vector: AccessVector::Network,
//!               access_complexity: AccessComplexity::Low,
//!               authentication: Authentication::None,
//!               confidentiality_impact: ConfidentialityImpact::None,
//!               integrity_impact: IntegrityImpact::None,
//!               availability_impact: AvailabilityImpact::Complete,
//!           },
//!           temporal: Some(TemporalVector {
//!               exploitability: Some(Exploitability::Functional),
//!               remediation_level: Some(RemediationLevel::OfficialFix),
//!               report_confidence: Some(ReportConfidence::Confirmed),
//!           }),
//!           environmental: Some(EnvironmentalVector {
//!               collateral_damage_potential: Some(CollateralDamagePotential::High),
//!               target_distribution: Some(TargetDistribution::High),
//!               confidentiality_requirement: Some(ConfidentialityRequirement::Medium),
//!               integrity_requirement: Some(IntegrityRequirement::Medium),
//!               availability_requirement: Some(AvailabilityRequirement::High),
//!           }),
//!       };
//! assert_eq!(Ok(cvss_vector), CVSS2Vector::parse_strict(cvss_vector_as_str));
//! assert_eq!(cvss_vector_as_str, format!("{}", cvss_vector));
//! assert_eq!(9.2, cvss_vector.score());
//! ```
//! # How to contribute
//! Please use [Inria's Gitlab](https://gitlab.inria.fr/celbaz/libcvss) to contribute to libcvss development.

#![cfg_attr(feature = "fail-on-warnings", deny(warnings))]

#[doc(hidden)]
pub mod parsing;
pub mod v2;
pub mod v3;
pub mod v31;
