//! Base vector implementation.

use crate::parsing;
use crate::parsing::{Field, Parser};
use serde::{Deserialize, Serialize};
use std::fmt::{Error, Formatter};
use std::str::Split;

/// A CVSS V2.0 base vector.
///
/// Per the CVSS specification, this structure contains an Access Vector field, an Access Complexity field, an Authentication field, a Confidentiality Impact field, an Integrity Impact field, and an Availability Impact field.
#[derive(Debug, Clone, Copy, PartialEq, Deserialize, Serialize)]
pub struct BaseVector {
    /// The Access Vector field as defined by the CVSS specification.
    pub access_vector: AccessVector,
    /// The Access Complexity field as defined by the CVSS specification.
    pub access_complexity: AccessComplexity,
    /// The Authentication field as defined by the CVSS specification.
    pub authentication: Authentication,
    /// The Confidentiality Impact field as defined by the CVSS specification.
    pub confidentiality_impact: ConfidentialityImpact,
    /// The Integrity Impact field as defined by the CVSS specification.
    pub integrity_impact: IntegrityImpact,
    /// The Availability Impact field as defined by the CVSS specification.
    pub availability_impact: AvailabilityImpact,
}

/// The Access Vector field as defined by the CVSS specification.
#[derive(Debug, Clone, Copy, PartialEq, Deserialize, Serialize)]
pub enum AccessVector {
    /// Local ("AV:L") value for the Access Vector field.
    Local,
    /// Adjacent Network ("AV:A") value for the Access Vector field.
    AdjacentNetwork,
    /// Network ("AV:N") value for the Access Vector field.
    Network,
}

/// The Access Complexity field as defined by the CVSS specification.
#[derive(Debug, Clone, Copy, PartialEq, Deserialize, Serialize)]
pub enum AccessComplexity {
    /// High ("AC:H") value for the Access Complexity field.
    High,
    /// Medium ("AC:M") value for the Access Complexity field.
    Medium,
    /// Low ("AC:L") value for the Access Complexity field.
    Low,
}

/// The Authentication field as defined by the CVSS specification.
#[derive(Debug, Clone, Copy, PartialEq, Deserialize, Serialize)]
pub enum Authentication {
    /// Multiple ("Au:M") value for the Authentication field.
    Multiple,
    /// Single ("Au:S") value for the Authentication field.
    Single,
    /// None ("Au:N") value for the Authentication field.
    None,
}

/// The Confidentiality Impact field as defined by the CVSS specification.
#[derive(Debug, Clone, Copy, PartialEq, Deserialize, Serialize)]
pub enum ConfidentialityImpact {
    /// None ("C:N") value for the Confidentiality Impact field.
    None,
    /// Partial ("C:P") value for the Confidentiality Impact field.
    Partial,
    /// Complete ("C:C") value for the Confidentiality Impact field.
    Complete,
}

/// The Integrity Impact field as defined by the CVSS specification.
#[derive(Debug, Clone, Copy, PartialEq, Deserialize, Serialize)]
pub enum IntegrityImpact {
    /// None ("I:N") value for the Integrity Impact field.
    None,
    /// Partial ("I:P") value for the Integrity Impact field.
    Partial,
    /// Complete ("I:C") value for the Integrity Impact field.
    Complete,
}

/// The Availability Impact field as defined by the CVSS specification.
#[derive(Debug, Clone, Copy, PartialEq, Deserialize, Serialize)]
pub enum AvailabilityImpact {
    /// None ("A:N") value for the Availability Impact field.
    None,
    /// Partial ("A:P") value for the Availability Impact field.
    Partial,
    /// Complete ("A:C") value for the Availability Impact field.
    Complete,
}

impl BaseVector {
    /// Provides the severity score for the CVSS base vector.
    ///
    /// Calling this method is identical to calling [`CVSS2Vector.score()`] when [`CVSS2Vector.temporal`] and [`CVSS2Vector.environmental`] are set to [`None`].
    ///
    /// [`CVSS2Vector.score()`]: ../struct.CVSS2Vector.html#method.score
    /// [`CVSS2Vector.temporal`]: ../struct.CVSS2Vector.html#structfield.temporal
    /// [`CVSS2Vector.environmental`]: ../struct.CVSS2Vector.html#structfield.environmental
    /// [`None`]: https://doc.rust-lang.org/std/option/enum.Option.html#variant.None
    pub fn score(&self) -> f64 {
        let impact = 10.41
            * (1.0
                - (1.0 - self.confidentiality_impact.numerical_value())
                    * (1.0 - self.integrity_impact.numerical_value())
                    * (1.0 - self.availability_impact.numerical_value()));

        self.score_with_adjusted_impact(impact)
    }

    #[doc(hidden)]
    pub fn score_with_adjusted_impact(&self, impact: f64) -> f64 {
        let exploitability = 20.0
            * self.access_vector.numerical_value()
            * self.access_complexity.numerical_value()
            * self.authentication.numerical_value();

        (10.0 * (((0.6 * impact) + (0.4 * exploitability) - 1.5) * BaseVector::f(impact))).round()
            / 10.0
    }

    fn f(impact: f64) -> f64 {
        if impact == 0.0 {
            0.0
        } else {
            1.176
        }
    }
}

impl Parser for BaseVector {
    fn parse_strict(split: &mut Box<Split<char>>) -> Result<Self, Vec<&'static str>>
    where
        Self: Sized,
    {
        let mut parsers = (None, None, None, None, None, None);
        let mut errors = Vec::new();

        let res = AccessVector::parse_strict(split);
        if res.is_ok() {
            parsers.0 = res.ok();
        } else {
            errors.append(&mut res.unwrap_err());
        }

        let res = AccessComplexity::parse_strict(split);
        if res.is_ok() {
            parsers.1 = res.ok();
        } else {
            errors.append(&mut res.unwrap_err());
        }

        let res = Authentication::parse_strict(split);
        if res.is_ok() {
            parsers.2 = res.ok();
        } else {
            errors.append(&mut res.unwrap_err());
        }

        let res = ConfidentialityImpact::parse_strict(split);
        if res.is_ok() {
            parsers.3 = res.ok();
        } else {
            errors.append(&mut res.unwrap_err());
        }

        let res = IntegrityImpact::parse_strict(split);
        if res.is_ok() {
            parsers.4 = res.ok();
        } else {
            errors.append(&mut res.unwrap_err());
        }

        let res = AvailabilityImpact::parse_strict(split);
        if res.is_ok() {
            parsers.5 = res.ok();
        } else {
            errors.append(&mut res.unwrap_err());
        }

        if parsers.0.is_some()
            && parsers.1.is_some()
            && parsers.2.is_some()
            && parsers.3.is_some()
            && parsers.4.is_some()
            && parsers.5.is_some()
        {
            Ok(BaseVector {
                access_vector: parsers.0.unwrap(),
                access_complexity: parsers.1.unwrap(),
                authentication: parsers.2.unwrap(),
                confidentiality_impact: parsers.3.unwrap(),
                integrity_impact: parsers.4.unwrap(),
                availability_impact: parsers.5.unwrap(),
            })
        } else {
            Err(errors)
        }
    }

    fn parse_nonstrict(split: &Box<Split<char>>) -> Result<Self, Vec<&'static str>>
    where
        Self: Sized,
    {
        let mut parsers = (None, None, None, None, None, None);
        let mut errors = Vec::new();

        let res = AccessVector::parse_nonstrict(split);
        if res.is_ok() {
            parsers.0 = res.ok();
        } else {
            errors.append(&mut res.unwrap_err());
        }

        let res = AccessComplexity::parse_nonstrict(split);
        if res.is_ok() {
            parsers.1 = res.ok();
        } else {
            errors.append(&mut res.unwrap_err());
        }

        let res = Authentication::parse_nonstrict(split);
        if res.is_ok() {
            parsers.2 = res.ok();
        } else {
            errors.append(&mut res.unwrap_err());
        }

        let res = ConfidentialityImpact::parse_nonstrict(split);
        if res.is_ok() {
            parsers.3 = res.ok();
        } else {
            errors.append(&mut res.unwrap_err());
        }

        let res = IntegrityImpact::parse_nonstrict(split);
        if res.is_ok() {
            parsers.4 = res.ok();
        } else {
            errors.append(&mut res.unwrap_err());
        }

        let res = AvailabilityImpact::parse_nonstrict(split);
        if res.is_ok() {
            parsers.5 = res.ok();
        } else {
            errors.append(&mut res.unwrap_err());
        }

        if parsers.0.is_some()
            && parsers.1.is_some()
            && parsers.2.is_some()
            && parsers.3.is_some()
            && parsers.4.is_some()
            && parsers.5.is_some()
        {
            Ok(BaseVector {
                access_vector: parsers.0.unwrap(),
                access_complexity: parsers.1.unwrap(),
                authentication: parsers.2.unwrap(),
                confidentiality_impact: parsers.3.unwrap(),
                integrity_impact: parsers.4.unwrap(),
                availability_impact: parsers.5.unwrap(),
            })
        } else {
            Err(errors)
        }
    }
}

impl AccessVector {
    /// Provides the numerical value associated with the metric, as specified by the CVSS specification.
    ///
    /// In normal usage you should not need to call this yourself.
    pub fn numerical_value(&self) -> f64 {
        match *self {
            AccessVector::Local => 0.395,
            AccessVector::AdjacentNetwork => 0.646,
            AccessVector::Network => 1.0,
        }
    }
}

impl Field for AccessVector {
    fn abbreviated_form(&self) -> &'static str {
        match *self {
            AccessVector::Local => "AV:L",
            AccessVector::AdjacentNetwork => "AV:A",
            AccessVector::Network => "AV:N",
        }
    }

    fn error_message() -> &'static str {
        "Access Vector (AV) could not be found."
    }

    fn parse(input: &str) -> Option<AccessVector> {
        if input == AccessVector::Local.abbreviated_form() {
            Some(AccessVector::Local)
        } else if input == AccessVector::AdjacentNetwork.abbreviated_form() {
            Some(AccessVector::AdjacentNetwork)
        } else if input == AccessVector::Network.abbreviated_form() {
            Some(AccessVector::Network)
        } else {
            None
        }
    }
}

impl Parser for AccessVector {
    fn parse_strict(split: &mut Box<Split<char>>) -> Result<Self, Vec<&'static str>>
    where
        Self: Sized,
    {
        parsing::parse_strict_default_field(split)
    }

    fn parse_nonstrict(split: &Box<Split<char>>) -> Result<Self, Vec<&'static str>>
    where
        Self: Sized,
    {
        parsing::parse_nonstrict_default_field(split)
    }
}

impl AccessComplexity {
    /// Provides the numerical value associated with the metric, as specified by the CVSS specification.
    ///
    /// In normal usage you should not need to call this yourself.
    pub fn numerical_value(&self) -> f64 {
        match *self {
            AccessComplexity::High => 0.35,
            AccessComplexity::Medium => 0.61,
            AccessComplexity::Low => 0.71,
        }
    }
}

impl Field for AccessComplexity {
    fn abbreviated_form(&self) -> &'static str {
        match *self {
            AccessComplexity::High => "AC:H",
            AccessComplexity::Medium => "AC:M",
            AccessComplexity::Low => "AC:L",
        }
    }

    fn error_message() -> &'static str {
        "Access Complexity (AC) could not be found."
    }

    fn parse(input: &str) -> Option<AccessComplexity> {
        if input == AccessComplexity::High.abbreviated_form() {
            Some(AccessComplexity::High)
        } else if input == AccessComplexity::Medium.abbreviated_form() {
            Some(AccessComplexity::Medium)
        } else if input == AccessComplexity::Low.abbreviated_form() {
            Some(AccessComplexity::Low)
        } else {
            None
        }
    }
}

impl Parser for AccessComplexity {
    fn parse_strict(split: &mut Box<Split<char>>) -> Result<Self, Vec<&'static str>>
    where
        Self: Sized,
    {
        parsing::parse_strict_default_field(split)
    }

    fn parse_nonstrict(split: &Box<Split<char>>) -> Result<Self, Vec<&'static str>>
    where
        Self: Sized,
    {
        parsing::parse_nonstrict_default_field(split)
    }
}

impl Authentication {
    /// Provides the numerical value associated with the metric, as specified by the CVSS specification.
    ///
    /// In normal usage you should not need to call this yourself.
    pub fn numerical_value(&self) -> f64 {
        match *self {
            Authentication::Multiple => 0.45,
            Authentication::Single => 0.56,
            Authentication::None => 0.704,
        }
    }
}

impl Field for Authentication {
    fn abbreviated_form(&self) -> &'static str {
        match *self {
            Authentication::Multiple => "Au:M",
            Authentication::Single => "Au:S",
            Authentication::None => "Au:N",
        }
    }

    fn error_message() -> &'static str {
        "Authentication (Au) could not be found."
    }

    fn parse(input: &str) -> Option<Authentication> {
        if input == Authentication::Multiple.abbreviated_form() {
            Some(Authentication::Multiple)
        } else if input == Authentication::Single.abbreviated_form() {
            Some(Authentication::Single)
        } else if input == Authentication::None.abbreviated_form() {
            Some(Authentication::None)
        } else {
            None
        }
    }
}

impl Parser for Authentication {
    fn parse_strict(split: &mut Box<Split<char>>) -> Result<Self, Vec<&'static str>>
    where
        Self: Sized,
    {
        parsing::parse_strict_default_field(split)
    }

    fn parse_nonstrict(split: &Box<Split<char>>) -> Result<Self, Vec<&'static str>>
    where
        Self: Sized,
    {
        parsing::parse_nonstrict_default_field(split)
    }
}

impl ConfidentialityImpact {
    /// Provides the numerical value associated with the metric, as specified by the CVSS specification.
    ///
    /// In normal usage you should not need to call this yourself.
    pub fn numerical_value(&self) -> f64 {
        match *self {
            ConfidentialityImpact::None => 0.0,
            ConfidentialityImpact::Partial => 0.275,
            ConfidentialityImpact::Complete => 0.66,
        }
    }
}

impl Field for ConfidentialityImpact {
    fn abbreviated_form(&self) -> &'static str {
        match *self {
            ConfidentialityImpact::None => "C:N",
            ConfidentialityImpact::Partial => "C:P",
            ConfidentialityImpact::Complete => "C:C",
        }
    }

    fn error_message() -> &'static str {
        "Confidentiality Impact (C) could not be found."
    }

    fn parse(input: &str) -> Option<ConfidentialityImpact> {
        if input == ConfidentialityImpact::None.abbreviated_form() {
            Some(ConfidentialityImpact::None)
        } else if input == ConfidentialityImpact::Partial.abbreviated_form() {
            Some(ConfidentialityImpact::Partial)
        } else if input == ConfidentialityImpact::Complete.abbreviated_form() {
            Some(ConfidentialityImpact::Complete)
        } else {
            None
        }
    }
}

impl Parser for ConfidentialityImpact {
    fn parse_strict(split: &mut Box<Split<char>>) -> Result<Self, Vec<&'static str>>
    where
        Self: Sized,
    {
        parsing::parse_strict_default_field(split)
    }

    fn parse_nonstrict(split: &Box<Split<char>>) -> Result<Self, Vec<&'static str>>
    where
        Self: Sized,
    {
        parsing::parse_nonstrict_default_field(split)
    }
}

impl IntegrityImpact {
    /// Provides the numerical value associated with the metric, as specified by the CVSS specification.
    ///
    /// In normal usage you should not need to call this yourself.
    pub fn numerical_value(&self) -> f64 {
        match *self {
            IntegrityImpact::None => 0.0,
            IntegrityImpact::Partial => 0.275,
            IntegrityImpact::Complete => 0.66,
        }
    }
}

impl Field for IntegrityImpact {
    fn abbreviated_form(&self) -> &'static str {
        match *self {
            IntegrityImpact::None => "I:N",
            IntegrityImpact::Partial => "I:P",
            IntegrityImpact::Complete => "I:C",
        }
    }

    fn error_message() -> &'static str {
        "Integrity Impact (C) could not be found."
    }

    fn parse(input: &str) -> Option<IntegrityImpact> {
        if input == IntegrityImpact::None.abbreviated_form() {
            Some(IntegrityImpact::None)
        } else if input == IntegrityImpact::Partial.abbreviated_form() {
            Some(IntegrityImpact::Partial)
        } else if input == IntegrityImpact::Complete.abbreviated_form() {
            Some(IntegrityImpact::Complete)
        } else {
            None
        }
    }
}

impl Parser for IntegrityImpact {
    fn parse_strict(split: &mut Box<Split<char>>) -> Result<Self, Vec<&'static str>>
    where
        Self: Sized,
    {
        parsing::parse_strict_default_field(split)
    }

    fn parse_nonstrict(split: &Box<Split<char>>) -> Result<Self, Vec<&'static str>>
    where
        Self: Sized,
    {
        parsing::parse_nonstrict_default_field(split)
    }
}

impl AvailabilityImpact {
    /// Provides the numerical value associated with the metric, as specified by the CVSS specification.
    ///
    /// In normal usage you should not need to call this yourself.
    pub fn numerical_value(&self) -> f64 {
        match *self {
            AvailabilityImpact::None => 0.0,
            AvailabilityImpact::Partial => 0.275,
            AvailabilityImpact::Complete => 0.66,
        }
    }
}

impl Field for AvailabilityImpact {
    fn abbreviated_form(&self) -> &'static str {
        match *self {
            AvailabilityImpact::None => "A:N",
            AvailabilityImpact::Partial => "A:P",
            AvailabilityImpact::Complete => "A:C",
        }
    }

    fn error_message() -> &'static str {
        "Availability Impact (C) could not be found."
    }

    fn parse(input: &str) -> Option<AvailabilityImpact> {
        if input == AvailabilityImpact::None.abbreviated_form() {
            Some(AvailabilityImpact::None)
        } else if input == AvailabilityImpact::Partial.abbreviated_form() {
            Some(AvailabilityImpact::Partial)
        } else if input == AvailabilityImpact::Complete.abbreviated_form() {
            Some(AvailabilityImpact::Complete)
        } else {
            None
        }
    }
}

impl Parser for AvailabilityImpact {
    fn parse_strict(split: &mut Box<Split<char>>) -> Result<Self, Vec<&'static str>>
    where
        Self: Sized,
    {
        parsing::parse_strict_default_field(split)
    }

    fn parse_nonstrict(split: &Box<Split<char>>) -> Result<Self, Vec<&'static str>>
    where
        Self: Sized,
    {
        parsing::parse_nonstrict_default_field(split)
    }
}

impl std::fmt::Display for AccessVector {
    fn fmt(&self, f: &mut Formatter) -> Result<(), Error> {
        write!(f, "{}", self.abbreviated_form())
    }
}

impl std::fmt::Display for AccessComplexity {
    fn fmt(&self, f: &mut Formatter) -> Result<(), Error> {
        write!(f, "{}", self.abbreviated_form())
    }
}

impl std::fmt::Display for Authentication {
    fn fmt(&self, f: &mut Formatter) -> Result<(), Error> {
        write!(f, "{}", self.abbreviated_form())
    }
}

impl std::fmt::Display for ConfidentialityImpact {
    fn fmt(&self, f: &mut Formatter) -> Result<(), Error> {
        write!(f, "{}", self.abbreviated_form())
    }
}

impl std::fmt::Display for IntegrityImpact {
    fn fmt(&self, f: &mut Formatter) -> Result<(), Error> {
        write!(f, "{}", self.abbreviated_form())
    }
}

impl std::fmt::Display for AvailabilityImpact {
    fn fmt(&self, f: &mut Formatter) -> Result<(), Error> {
        write!(f, "{}", self.abbreviated_form())
    }
}

impl std::fmt::Display for BaseVector {
    fn fmt(&self, f: &mut Formatter) -> Result<(), Error> {
        write!(
            f,
            "{}/{}/{}/{}/{}/{}",
            self.access_vector,
            self.access_complexity,
            self.authentication,
            self.confidentiality_impact,
            self.integrity_impact,
            self.availability_impact
        )
    }
}

#[cfg(test)]
pub mod tests {
    use super::*;

    #[test]
    fn test_formatting() {
        assert_eq!("AV:L", format!("{}", AccessVector::Local));
        assert_eq!("AV:A", format!("{}", AccessVector::AdjacentNetwork));
        assert_eq!("AV:N", format!("{}", AccessVector::Network));

        assert_eq!("AC:H", format!("{}", AccessComplexity::High));
        assert_eq!("AC:M", format!("{}", AccessComplexity::Medium));
        assert_eq!("AC:L", format!("{}", AccessComplexity::Low));

        assert_eq!("Au:M", format!("{}", Authentication::Multiple));
        assert_eq!("Au:S", format!("{}", Authentication::Single));
        assert_eq!("Au:N", format!("{}", Authentication::None));

        assert_eq!("C:N", format!("{}", ConfidentialityImpact::None));
        assert_eq!("C:P", format!("{}", ConfidentialityImpact::Partial));
        assert_eq!("C:C", format!("{}", ConfidentialityImpact::Complete));

        assert_eq!("I:N", format!("{}", IntegrityImpact::None));
        assert_eq!("I:P", format!("{}", IntegrityImpact::Partial));
        assert_eq!("I:C", format!("{}", IntegrityImpact::Complete));

        assert_eq!("A:N", format!("{}", AvailabilityImpact::None));
        assert_eq!("A:P", format!("{}", AvailabilityImpact::Partial));
        assert_eq!("A:C", format!("{}", AvailabilityImpact::Complete));

        // CVE-2002-0392 see CVSS v2 specification section 3.3.1
        assert_eq!(
            "AV:N/AC:L/Au:N/C:N/I:N/A:C",
            format!("{}", provide_base_vector1())
        );
    }

    #[test]
    fn test_parsing() {
        // Access Vector
        assert_eq!(Some(AccessVector::Local), AccessVector::parse("AV:L"));
        assert_eq!(
            Some(AccessVector::AdjacentNetwork),
            AccessVector::parse("AV:A")
        );
        assert_eq!(Some(AccessVector::Network), AccessVector::parse("AV:N"));
        assert_eq!(None, AccessVector::parse("fslfjskfj"));
        assert_eq!(None, AccessVector::parse("AV:"));
        assert_eq!(None, AccessVector::parse("AV:LA"));

        // Access Complexity
        assert_eq!(
            Some(AccessComplexity::High),
            AccessComplexity::parse("AC:H")
        );
        assert_eq!(
            Some(AccessComplexity::Medium),
            AccessComplexity::parse("AC:M")
        );
        assert_eq!(Some(AccessComplexity::Low), AccessComplexity::parse("AC:L"));
        assert_eq!(None, AccessComplexity::parse("fslfjskfj"));
        assert_eq!(None, AccessComplexity::parse("AC:"));
        assert_eq!(None, AccessComplexity::parse("AC:LA"));

        // Authentication
        assert_eq!(
            Some(Authentication::Multiple),
            Authentication::parse("Au:M")
        );
        assert_eq!(Some(Authentication::Single), Authentication::parse("Au:S"));
        assert_eq!(Some(Authentication::None), Authentication::parse("Au:N"));
        assert_eq!(None, Authentication::parse("fslfjskfj"));
        assert_eq!(None, Authentication::parse("Au:"));
        assert_eq!(None, Authentication::parse("Au:NA"));

        // Confidentiality
        assert_eq!(
            Some(ConfidentialityImpact::None),
            ConfidentialityImpact::parse("C:N")
        );
        assert_eq!(
            Some(ConfidentialityImpact::Partial),
            ConfidentialityImpact::parse("C:P")
        );
        assert_eq!(
            Some(ConfidentialityImpact::Complete),
            ConfidentialityImpact::parse("C:C")
        );
        assert_eq!(None, ConfidentialityImpact::parse("fslfjskfj"));
        assert_eq!(None, ConfidentialityImpact::parse("C:"));
        assert_eq!(None, ConfidentialityImpact::parse("C:NA"));

        // Integrity
        assert_eq!(Some(IntegrityImpact::None), IntegrityImpact::parse("I:N"));
        assert_eq!(
            Some(IntegrityImpact::Partial),
            IntegrityImpact::parse("I:P")
        );
        assert_eq!(
            Some(IntegrityImpact::Complete),
            IntegrityImpact::parse("I:C")
        );
        assert_eq!(None, IntegrityImpact::parse("fslfjskfj"));
        assert_eq!(None, IntegrityImpact::parse("I:"));
        assert_eq!(None, IntegrityImpact::parse("I:NA"));

        // Availability
        assert_eq!(
            Some(AvailabilityImpact::None),
            AvailabilityImpact::parse("A:N")
        );
        assert_eq!(
            Some(AvailabilityImpact::Partial),
            AvailabilityImpact::parse("A:P")
        );
        assert_eq!(
            Some(AvailabilityImpact::Complete),
            AvailabilityImpact::parse("A:C")
        );
        assert_eq!(None, AvailabilityImpact::parse("fslfjskfj"));
        assert_eq!(None, AvailabilityImpact::parse("A:"));
        assert_eq!(None, AvailabilityImpact::parse("A:NA"));

        // CVE-2002-0392 see CVSS v2 specification section 3.3.1
        assert_eq!(
            Ok(provide_base_vector1()),
            BaseVector::parse_strict(&mut Box::new("AV:N/AC:L/Au:N/C:N/I:N/A:C".split('/')))
        );
        assert_eq!(
            Ok(provide_base_vector1()),
            BaseVector::parse_nonstrict(&mut Box::new("AV:N/AC:L/Au:N/C:N/I:N/A:C".split('/')))
        );

        // With one field swapped
        assert!(
            BaseVector::parse_strict(&mut Box::new("AC:L/Au:N/AV:N/C:N/I:N/A:C".split('/')))
                .is_err()
        );
        assert_eq!(
            Ok(provide_base_vector1()),
            BaseVector::parse_nonstrict(&mut Box::new("AC:L/Au:N/AV:N/C:N/I:N/A:C".split('/')))
        );

        // With one mandatory field missing
        assert!(
            BaseVector::parse_strict(&mut Box::new("AC:L/Au:N/C:N/I:N/A:C".split('/'))).is_err()
        );
        assert!(
            BaseVector::parse_strict(&mut Box::new("AC:L/Au:N/C:N/I:N/A:C".split('/'))).is_err()
        );

        // Junk entry
        assert!(BaseVector::parse_strict(&mut Box::new("fsjfskhf".split('/'))).is_err());
        assert!(BaseVector::parse_nonstrict(&mut Box::new("fsjfskhf".split('/'))).is_err());

        assert!(BaseVector::parse_strict(&mut Box::new("fs//jf/skhf".split('/'))).is_err());
        assert!(BaseVector::parse_nonstrict(&mut Box::new("fs//jf/skhf".split('/'))).is_err());
    }

    #[test]
    fn test_scoring() {
        assert_eq!(7.8, provide_base_vector1().score());
    }

    // CVE-2002-0392 see CVSS v2 specification section 3.3.1
    fn provide_base_vector1() -> BaseVector {
        BaseVector {
            access_vector: AccessVector::Network,
            access_complexity: AccessComplexity::Low,
            authentication: Authentication::None,
            confidentiality_impact: ConfidentialityImpact::None,
            integrity_impact: IntegrityImpact::None,
            availability_impact: AvailabilityImpact::Complete,
        }
    }
}
