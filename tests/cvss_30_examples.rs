use libcvss::v3::base::exploitability::{
    AttackComplexity, AttackVector, Exploitability, PrivilegesRequired, UserInteraction,
};
use libcvss::v3::base::impact::{Availability, Confidentiality, Impact, Integrity};
use libcvss::v3::base::scope::Scope;
use libcvss::v3::base::BaseVector;
use libcvss::v3::CVSS3Vector;

// These test cases are based on the official CVSS 3.0 examples: https://www.first.org/cvss/v3.0/examples

struct TestCase {
    cvss_vector: CVSS3Vector,
    base_score: f64,
    cvss_string: &'static str,
}

#[test]
pub fn cvss_3_examples() {
    let test_cases = vec![
        cve_2013_1937(),
        cve_2013_0375(),
        cve_2014_3566(),
        cve_2012_1516(),
        cve_2009_0783(),
        cve_2012_0384(),
        cve_2015_1098(),
        cve_2014_0160(),
        cve_2014_6271(),
        cve_2008_1447(),
        cve_2014_2005(),
        cve_2010_0467(),
        cve_2012_1342(),
        cve_2013_6014(),
        cve_2014_9253(),
        cve_2009_0658(),
        cve_2011_1265(),
        cve_2014_2019(),
        cve_2015_0970(),
        cve_2014_0224(),
        cve_2012_5376(),
        cve_2016_1645(),
        cve_2016_0128(),
        cve_2016_2118(),
    ];

    for test_case in test_cases {
        validate_test_case(&test_case);
    }
}

fn validate_test_case(test_case: &TestCase) {
    assert_eq!(test_case.cvss_string, format!("{}", test_case.cvss_vector));
    assert_eq!(
        Ok(test_case.cvss_vector),
        CVSS3Vector::parse_strict(test_case.cvss_string)
    );
    assert_eq!(
        Ok(test_case.cvss_vector),
        CVSS3Vector::parse_nonstrict(test_case.cvss_string)
    );
    assert_eq!(test_case.base_score, test_case.cvss_vector.score())
}

fn cve_2013_1937() -> TestCase {
    TestCase {
        cvss_vector: CVSS3Vector {
            base: BaseVector {
                exploitability: Exploitability {
                    attack_vector: AttackVector::Network,
                    attack_complexity: AttackComplexity::Low,
                    privileges_required: PrivilegesRequired::None,
                    user_interaction: UserInteraction::Required,
                },
                scope: Scope::Changed,
                impact: Impact {
                    confidentiality: Confidentiality::Low,
                    integrity: Integrity::Low,
                    availability: Availability::None,
                },
            },
            temporal: None,
            environmental: None,
        },
        base_score: 6.1,
        cvss_string: "CVSS:3.0/AV:N/AC:L/PR:N/UI:R/S:C/C:L/I:L/A:N", // Computed using https://www.first.org/cvss/calculator/3.0
    }
}

fn cve_2013_0375() -> TestCase {
    TestCase {
        cvss_vector: CVSS3Vector {
            base: BaseVector {
                exploitability: Exploitability {
                    attack_vector: AttackVector::Network,
                    attack_complexity: AttackComplexity::Low,
                    privileges_required: PrivilegesRequired::Low,
                    user_interaction: UserInteraction::None,
                },
                scope: Scope::Changed,
                impact: Impact {
                    confidentiality: Confidentiality::Low,
                    integrity: Integrity::Low,
                    availability: Availability::None,
                },
            },
            temporal: None,
            environmental: None,
        },
        base_score: 6.4,
        cvss_string: "CVSS:3.0/AV:N/AC:L/PR:L/UI:N/S:C/C:L/I:L/A:N", // Computed using https://www.first.org/cvss/calculator/3.0
    }
}

fn cve_2014_3566() -> TestCase {
    TestCase {
        cvss_vector: CVSS3Vector {
            base: BaseVector {
                exploitability: Exploitability {
                    attack_vector: AttackVector::Network,
                    attack_complexity: AttackComplexity::High,
                    privileges_required: PrivilegesRequired::None,
                    user_interaction: UserInteraction::Required,
                },
                scope: Scope::Unchanged,
                impact: Impact {
                    confidentiality: Confidentiality::Low,
                    integrity: Integrity::None,
                    availability: Availability::None,
                },
            },
            temporal: None,
            environmental: None,
        },
        base_score: 3.1,
        cvss_string: "CVSS:3.0/AV:N/AC:H/PR:N/UI:R/S:U/C:L/I:N/A:N", // Computed using https://www.first.org/cvss/calculator/3.0
    }
}

fn cve_2012_1516() -> TestCase {
    TestCase {
        cvss_vector: CVSS3Vector {
            base: BaseVector {
                exploitability: Exploitability {
                    attack_vector: AttackVector::Network,
                    attack_complexity: AttackComplexity::Low,
                    privileges_required: PrivilegesRequired::Low,
                    user_interaction: UserInteraction::None,
                },
                scope: Scope::Changed,
                impact: Impact {
                    confidentiality: Confidentiality::High,
                    integrity: Integrity::High,
                    availability: Availability::High,
                },
            },
            temporal: None,
            environmental: None,
        },
        base_score: 9.9,
        cvss_string: "CVSS:3.0/AV:N/AC:L/PR:L/UI:N/S:C/C:H/I:H/A:H", // Computed using https://www.first.org/cvss/calculator/3.0
    }
}

fn cve_2009_0783() -> TestCase {
    TestCase {
        cvss_vector: CVSS3Vector {
            base: BaseVector {
                exploitability: Exploitability {
                    attack_vector: AttackVector::Local,
                    attack_complexity: AttackComplexity::Low,
                    privileges_required: PrivilegesRequired::High,
                    user_interaction: UserInteraction::None,
                },
                scope: Scope::Unchanged,
                impact: Impact {
                    confidentiality: Confidentiality::Low,
                    integrity: Integrity::Low,
                    availability: Availability::Low,
                },
            },
            temporal: None,
            environmental: None,
        },
        base_score: 4.2,
        cvss_string: "CVSS:3.0/AV:L/AC:L/PR:H/UI:N/S:U/C:L/I:L/A:L", // Computed using https://www.first.org/cvss/calculator/3.0
    }
}

fn cve_2012_0384() -> TestCase {
    TestCase {
        cvss_vector: CVSS3Vector {
            base: BaseVector {
                exploitability: Exploitability {
                    attack_vector: AttackVector::Network,
                    attack_complexity: AttackComplexity::Low,
                    privileges_required: PrivilegesRequired::Low,
                    user_interaction: UserInteraction::None,
                },
                scope: Scope::Unchanged,
                impact: Impact {
                    confidentiality: Confidentiality::High,
                    integrity: Integrity::High,
                    availability: Availability::High,
                },
            },
            temporal: None,
            environmental: None,
        },
        base_score: 8.8,
        cvss_string: "CVSS:3.0/AV:N/AC:L/PR:L/UI:N/S:U/C:H/I:H/A:H", // Computed using https://www.first.org/cvss/calculator/3.0
    }
}

fn cve_2015_1098() -> TestCase {
    TestCase {
        cvss_vector: CVSS3Vector {
            base: BaseVector {
                exploitability: Exploitability {
                    attack_vector: AttackVector::Local,
                    attack_complexity: AttackComplexity::Low,
                    privileges_required: PrivilegesRequired::None,
                    user_interaction: UserInteraction::Required,
                },
                scope: Scope::Unchanged,
                impact: Impact {
                    confidentiality: Confidentiality::High,
                    integrity: Integrity::High,
                    availability: Availability::High,
                },
            },
            temporal: None,
            environmental: None,
        },
        base_score: 7.8,
        cvss_string: "CVSS:3.0/AV:L/AC:L/PR:N/UI:R/S:U/C:H/I:H/A:H", // Computed using https://www.first.org/cvss/calculator/3.0
    }
}

fn cve_2014_0160() -> TestCase {
    TestCase {
        cvss_vector: CVSS3Vector {
            base: BaseVector {
                exploitability: Exploitability {
                    attack_vector: AttackVector::Network,
                    attack_complexity: AttackComplexity::Low,
                    privileges_required: PrivilegesRequired::None,
                    user_interaction: UserInteraction::None,
                },
                scope: Scope::Unchanged,
                impact: Impact {
                    confidentiality: Confidentiality::High,
                    integrity: Integrity::None,
                    availability: Availability::None,
                },
            },
            temporal: None,
            environmental: None,
        },
        base_score: 7.5,
        cvss_string: "CVSS:3.0/AV:N/AC:L/PR:N/UI:N/S:U/C:H/I:N/A:N", // Computed using https://www.first.org/cvss/calculator/3.0
    }
}

fn cve_2014_6271() -> TestCase {
    TestCase {
        cvss_vector: CVSS3Vector {
            base: BaseVector {
                exploitability: Exploitability {
                    attack_vector: AttackVector::Network,
                    attack_complexity: AttackComplexity::Low,
                    privileges_required: PrivilegesRequired::None,
                    user_interaction: UserInteraction::None,
                },
                scope: Scope::Unchanged,
                impact: Impact {
                    confidentiality: Confidentiality::High,
                    integrity: Integrity::High,
                    availability: Availability::High,
                },
            },
            temporal: None,
            environmental: None,
        },
        base_score: 9.8,
        cvss_string: "CVSS:3.0/AV:N/AC:L/PR:N/UI:N/S:U/C:H/I:H/A:H", // Computed using https://www.first.org/cvss/calculator/3.0
    }
}

fn cve_2008_1447() -> TestCase {
    TestCase {
        cvss_vector: CVSS3Vector {
            base: BaseVector {
                exploitability: Exploitability {
                    attack_vector: AttackVector::Network,
                    attack_complexity: AttackComplexity::High,
                    privileges_required: PrivilegesRequired::None,
                    user_interaction: UserInteraction::None,
                },
                scope: Scope::Changed,
                impact: Impact {
                    confidentiality: Confidentiality::None,
                    integrity: Integrity::High,
                    availability: Availability::None,
                },
            },
            temporal: None,
            environmental: None,
        },
        base_score: 6.8,
        cvss_string: "CVSS:3.0/AV:N/AC:H/PR:N/UI:N/S:C/C:N/I:H/A:N", // Computed using https://www.first.org/cvss/calculator/3.0
    }
}

fn cve_2014_2005() -> TestCase {
    TestCase {
        cvss_vector: CVSS3Vector {
            base: BaseVector {
                exploitability: Exploitability {
                    attack_vector: AttackVector::Physical,
                    attack_complexity: AttackComplexity::Low,
                    privileges_required: PrivilegesRequired::None,
                    user_interaction: UserInteraction::None,
                },
                scope: Scope::Unchanged,
                impact: Impact {
                    confidentiality: Confidentiality::High,
                    integrity: Integrity::High,
                    availability: Availability::High,
                },
            },
            temporal: None,
            environmental: None,
        },
        base_score: 6.8,
        cvss_string: "CVSS:3.0/AV:P/AC:L/PR:N/UI:N/S:U/C:H/I:H/A:H", // Computed using https://www.first.org/cvss/calculator/3.0
    }
}

fn cve_2010_0467() -> TestCase {
    TestCase {
        cvss_vector: CVSS3Vector {
            base: BaseVector {
                exploitability: Exploitability {
                    attack_vector: AttackVector::Network,
                    attack_complexity: AttackComplexity::Low,
                    privileges_required: PrivilegesRequired::None,
                    user_interaction: UserInteraction::None,
                },
                scope: Scope::Changed,
                impact: Impact {
                    confidentiality: Confidentiality::Low,
                    integrity: Integrity::None,
                    availability: Availability::None,
                },
            },
            temporal: None,
            environmental: None,
        },
        base_score: 5.8,
        cvss_string: "CVSS:3.0/AV:N/AC:L/PR:N/UI:N/S:C/C:L/I:N/A:N", // Computed using https://www.first.org/cvss/calculator/3.0
    }
}

fn cve_2012_1342() -> TestCase {
    TestCase {
        cvss_vector: CVSS3Vector {
            base: BaseVector {
                exploitability: Exploitability {
                    attack_vector: AttackVector::Network,
                    attack_complexity: AttackComplexity::Low,
                    privileges_required: PrivilegesRequired::None,
                    user_interaction: UserInteraction::None,
                },
                scope: Scope::Changed,
                impact: Impact {
                    confidentiality: Confidentiality::None,
                    integrity: Integrity::Low,
                    availability: Availability::None,
                },
            },
            temporal: None,
            environmental: None,
        },
        base_score: 5.8,
        cvss_string: "CVSS:3.0/AV:N/AC:L/PR:N/UI:N/S:C/C:N/I:L/A:N", // Computed using https://www.first.org/cvss/calculator/3.0
    }
}
fn cve_2013_6014() -> TestCase {
    TestCase {
        cvss_vector: CVSS3Vector {
            base: BaseVector {
                exploitability: Exploitability {
                    attack_vector: AttackVector::Adjacent,
                    attack_complexity: AttackComplexity::Low,
                    privileges_required: PrivilegesRequired::None,
                    user_interaction: UserInteraction::None,
                },
                scope: Scope::Changed,
                impact: Impact {
                    confidentiality: Confidentiality::High,
                    integrity: Integrity::None,
                    availability: Availability::High,
                },
            },
            temporal: None,
            environmental: None,
        },
        base_score: 9.3,
        cvss_string: "CVSS:3.0/AV:A/AC:L/PR:N/UI:N/S:C/C:H/I:N/A:H", // Computed using https://www.first.org/cvss/calculator/3.0
    }
}

fn cve_2014_9253() -> TestCase {
    TestCase {
        cvss_vector: CVSS3Vector {
            base: BaseVector {
                exploitability: Exploitability {
                    attack_vector: AttackVector::Network,
                    attack_complexity: AttackComplexity::Low,
                    privileges_required: PrivilegesRequired::Low,
                    user_interaction: UserInteraction::Required,
                },
                scope: Scope::Changed,
                impact: Impact {
                    confidentiality: Confidentiality::Low,
                    integrity: Integrity::Low,
                    availability: Availability::None,
                },
            },
            temporal: None,
            environmental: None,
        },
        base_score: 5.4,
        cvss_string: "CVSS:3.0/AV:N/AC:L/PR:L/UI:R/S:C/C:L/I:L/A:N", // Computed using https://www.first.org/cvss/calculator/3.0
    }
}

fn cve_2009_0658() -> TestCase {
    TestCase {
        cvss_vector: CVSS3Vector {
            base: BaseVector {
                exploitability: Exploitability {
                    attack_vector: AttackVector::Local,
                    attack_complexity: AttackComplexity::Low,
                    privileges_required: PrivilegesRequired::None,
                    user_interaction: UserInteraction::Required,
                },
                scope: Scope::Unchanged,
                impact: Impact {
                    confidentiality: Confidentiality::High,
                    integrity: Integrity::High,
                    availability: Availability::High,
                },
            },
            temporal: None,
            environmental: None,
        },
        base_score: 7.8,
        cvss_string: "CVSS:3.0/AV:L/AC:L/PR:N/UI:R/S:U/C:H/I:H/A:H", // Computed using https://www.first.org/cvss/calculator/3.0
    }
}

fn cve_2011_1265() -> TestCase {
    TestCase {
        cvss_vector: CVSS3Vector {
            base: BaseVector {
                exploitability: Exploitability {
                    attack_vector: AttackVector::Adjacent,
                    attack_complexity: AttackComplexity::Low,
                    privileges_required: PrivilegesRequired::None,
                    user_interaction: UserInteraction::None,
                },
                scope: Scope::Unchanged,
                impact: Impact {
                    confidentiality: Confidentiality::High,
                    integrity: Integrity::High,
                    availability: Availability::High,
                },
            },
            temporal: None,
            environmental: None,
        },
        base_score: 8.8,
        cvss_string: "CVSS:3.0/AV:A/AC:L/PR:N/UI:N/S:U/C:H/I:H/A:H", // Computed using https://www.first.org/cvss/calculator/3.0
    }
}

fn cve_2014_2019() -> TestCase {
    TestCase {
        cvss_vector: CVSS3Vector {
            base: BaseVector {
                exploitability: Exploitability {
                    attack_vector: AttackVector::Physical,
                    attack_complexity: AttackComplexity::Low,
                    privileges_required: PrivilegesRequired::None,
                    user_interaction: UserInteraction::None,
                },
                scope: Scope::Unchanged,
                impact: Impact {
                    confidentiality: Confidentiality::None,
                    integrity: Integrity::High,
                    availability: Availability::None,
                },
            },
            temporal: None,
            environmental: None,
        },
        base_score: 4.6,
        cvss_string: "CVSS:3.0/AV:P/AC:L/PR:N/UI:N/S:U/C:N/I:H/A:N", // Computed using https://www.first.org/cvss/calculator/3.0
    }
}

fn cve_2015_0970() -> TestCase {
    TestCase {
        cvss_vector: CVSS3Vector {
            base: BaseVector {
                exploitability: Exploitability {
                    attack_vector: AttackVector::Network,
                    attack_complexity: AttackComplexity::Low,
                    privileges_required: PrivilegesRequired::None,
                    user_interaction: UserInteraction::Required,
                },
                scope: Scope::Unchanged,
                impact: Impact {
                    confidentiality: Confidentiality::High,
                    integrity: Integrity::High,
                    availability: Availability::High,
                },
            },
            temporal: None,
            environmental: None,
        },
        base_score: 8.8,
        cvss_string: "CVSS:3.0/AV:N/AC:L/PR:N/UI:R/S:U/C:H/I:H/A:H", // Computed using https://www.first.org/cvss/calculator/3.0
    }
}

fn cve_2014_0224() -> TestCase {
    TestCase {
        cvss_vector: CVSS3Vector {
            base: BaseVector {
                exploitability: Exploitability {
                    attack_vector: AttackVector::Network,
                    attack_complexity: AttackComplexity::High,
                    privileges_required: PrivilegesRequired::None,
                    user_interaction: UserInteraction::None,
                },
                scope: Scope::Unchanged,
                impact: Impact {
                    confidentiality: Confidentiality::High,
                    integrity: Integrity::High,
                    availability: Availability::None,
                },
            },
            temporal: None,
            environmental: None,
        },
        base_score: 7.4,
        cvss_string: "CVSS:3.0/AV:N/AC:H/PR:N/UI:N/S:U/C:H/I:H/A:N", // Computed using https://www.first.org/cvss/calculator/3.0
    }
}

fn cve_2012_5376() -> TestCase {
    TestCase {
        cvss_vector: CVSS3Vector {
            base: BaseVector {
                exploitability: Exploitability {
                    attack_vector: AttackVector::Network,
                    attack_complexity: AttackComplexity::Low,
                    privileges_required: PrivilegesRequired::None,
                    user_interaction: UserInteraction::Required,
                },
                scope: Scope::Changed,
                impact: Impact {
                    confidentiality: Confidentiality::High,
                    integrity: Integrity::High,
                    availability: Availability::High,
                },
            },
            temporal: None,
            environmental: None,
        },
        base_score: 9.6,
        cvss_string: "CVSS:3.0/AV:N/AC:L/PR:N/UI:R/S:C/C:H/I:H/A:H", // Computed using https://www.first.org/cvss/calculator/3.0
    }
}

fn cve_2016_1645() -> TestCase {
    TestCase {
        cvss_vector: CVSS3Vector {
            base: BaseVector {
                exploitability: Exploitability {
                    attack_vector: AttackVector::Network,
                    attack_complexity: AttackComplexity::Low,
                    privileges_required: PrivilegesRequired::None,
                    user_interaction: UserInteraction::Required,
                },
                scope: Scope::Unchanged,
                impact: Impact {
                    confidentiality: Confidentiality::High,
                    integrity: Integrity::High,
                    availability: Availability::High,
                },
            },
            temporal: None,
            environmental: None,
        },
        base_score: 8.8,
        cvss_string: "CVSS:3.0/AV:N/AC:L/PR:N/UI:R/S:U/C:H/I:H/A:H", // Computed using https://www.first.org/cvss/calculator/3.0
    }
}

fn cve_2016_0128() -> TestCase {
    TestCase {
        cvss_vector: CVSS3Vector {
            base: BaseVector {
                exploitability: Exploitability {
                    attack_vector: AttackVector::Network,
                    attack_complexity: AttackComplexity::High,
                    privileges_required: PrivilegesRequired::None,
                    user_interaction: UserInteraction::Required,
                },
                scope: Scope::Unchanged,
                impact: Impact {
                    confidentiality: Confidentiality::High,
                    integrity: Integrity::High,
                    availability: Availability::None,
                },
            },
            temporal: None,
            environmental: None,
        },
        base_score: 6.8,
        cvss_string: "CVSS:3.0/AV:N/AC:H/PR:N/UI:R/S:U/C:H/I:H/A:N", // Computed using https://www.first.org/cvss/calculator/3.0
    }
}

fn cve_2016_2118() -> TestCase {
    TestCase {
        cvss_vector: CVSS3Vector {
            base: BaseVector {
                exploitability: Exploitability {
                    attack_vector: AttackVector::Network,
                    attack_complexity: AttackComplexity::High,
                    privileges_required: PrivilegesRequired::None,
                    user_interaction: UserInteraction::Required,
                },
                scope: Scope::Unchanged,
                impact: Impact {
                    confidentiality: Confidentiality::High,
                    integrity: Integrity::High,
                    availability: Availability::High,
                },
            },
            temporal: None,
            environmental: None,
        },
        base_score: 7.5,
        cvss_string: "CVSS:3.0/AV:N/AC:H/PR:N/UI:R/S:U/C:H/I:H/A:H", // Computed using https://www.first.org/cvss/calculator/3.0
    }
}
